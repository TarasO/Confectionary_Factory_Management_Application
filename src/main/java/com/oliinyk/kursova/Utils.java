package com.oliinyk.kursova;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Utils {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static void showAlert(String title, String content, Alert.AlertType alertType){
        Alert alert = new Alert(alertType);

        Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("img/icon.png"));

        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }

    public static String formatDate(Date date) {
        return simpleDateFormat.format(date);
    }
}
