package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Position")
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column( length = 15, nullable = false)
    private String abbr;

    @Column(precision = 10, scale = 2, nullable = false)
    private double payment;

    @Column(name = "night_shift_bonus", nullable = false)
    private int nightShiftBonus;

    @Column(name = "loyalty_bonus", nullable = false)
    private int loyaltyBonus;

    @ManyToOne
    @JoinColumn(name = "rate_type_id", nullable = false)
    private RateType rateType;

    @OneToMany(mappedBy = "position")
    private List<Employee> employees;

    public Position(String name, String abbr, RateType rateType, double payment, int nightShiftBonus, int loyaltyBonus) {
        this.name = name;
        this.abbr = abbr;
        this.rateType = rateType;
        this.payment = payment;
        this.nightShiftBonus = nightShiftBonus;
        this.loyaltyBonus = loyaltyBonus;
    }

    @Override
    public String toString() {
        return name;
    }
}
