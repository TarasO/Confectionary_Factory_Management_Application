package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "StoredIngredient")
public class StoredIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = false)
    private Ingredient ingredient;

    @ManyToOne
    @JoinColumn(name = "storage_id", nullable = false)
    private IngredientStorage storage;

    public StoredIngredient(IngredientStorage storage, Ingredient ingredient, double quantity) {
        this.storage = storage;
        this.ingredient = ingredient;
        this.quantity = quantity;
    }
}
