package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "OrderStatus")
public class OrderStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 20, nullable = false)
    private String name;

    @OneToMany(mappedBy = "status")
    private List<SupplyOrder> supplyOrders;

    @OneToMany(mappedBy = "status")
    private List<ClientOrder> clientOrders;

    @Override
    public String toString() {
        return name;
    }
}
