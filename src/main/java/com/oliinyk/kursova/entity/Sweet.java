package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Sweet")
public class Sweet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 25, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "unit_id", nullable = false)
    private Unit unit;

    @OneToMany(mappedBy = "sweet")
    private List<SweetIngredient> ingredients;

    @OneToMany(mappedBy = "sweet")
    private List<StoredSweet> storages;

    @OneToMany(mappedBy = "sweet")
    private List<ClientOrderContent> clientOrderContents;

    @OneToMany(mappedBy = "sweet")
    private List<Production> productions;

    public Sweet(String name, Category category, Unit unit) {
        this.name = name;
        this.category = category;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return name;
    }
}
