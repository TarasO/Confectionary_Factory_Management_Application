package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IngredientStorage")
public class IngredientStorage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 75)
    private String address;

    @OneToMany(mappedBy = "storage", fetch = FetchType.EAGER)
    @ToString.Exclude
    private List<StoredIngredient> ingredients;

    public IngredientStorage(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return name;
    }
}
