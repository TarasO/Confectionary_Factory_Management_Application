package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Unit")
public class Unit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 20, nullable = false)
    private String name;

    @Column(length = 10, nullable = false)
    private String abbr;

    @OneToMany(mappedBy = "unit")
    private List<Ingredient> ingredients;

    @OneToMany(mappedBy = "unit")
    @ToString.Exclude
    private List<Sweet> sweets;

    public Unit(String name, String abbr) {
        this.name = name;
        this.abbr = abbr;
    }

    @Override
    public String toString() {
        return name;
    }
}
