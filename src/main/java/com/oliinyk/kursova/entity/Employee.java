package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(name = "last_name", length = 30, nullable = false)
    private String lastName;

    @Column(name = "first_name", length = 30, nullable = false)
    private String firstName;

    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Column(name = "phone_number", length = 15, nullable = false)
    private String phoneNumber;

    @Column(name = "begin_date", nullable = false)
    private Date beginDate;

    @Column(name = "end_date")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "position_id", nullable = false)
    private Position position;

    @ManyToOne
    @JoinColumn(name = "workshop_id", nullable = false)
    private Workshop workshop;

    @OneToMany(mappedBy = "employee")
    private List<ShiftEmployee> shifts;

    public Employee(String lastName, String firstName, Date birthDate, Position position, Workshop workshop, String phoneNumber, Date beginDate) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.position = position;
        this.workshop = workshop;
        this.phoneNumber = phoneNumber;
        this.beginDate = beginDate;
    }

    @Override
    public String toString() {
        return lastName + " " + firstName + " " + position.getName();
    }
}
