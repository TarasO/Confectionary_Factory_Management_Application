package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ClientOrder")
public class ClientOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 20, unique = true, nullable = false)
    private String identifier;

    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    @Column(name = "deadline_date", nullable = false)
    private Date deadlineDate;

    @Column(name = "delivery_date")
    private Date deliveryDate;

    @Column(nullable = false)
    private int discount;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private OrderStatus status;

    @OneToMany(mappedBy = "clientOrder", fetch = FetchType.EAGER)
    private List<ClientOrderContent> contents;

    public ClientOrder(String identifier, Client client, OrderStatus status, Date orderDate, Date deadlineDate, Date deliveryDate) {
        this.identifier = identifier;
        this.client = client;
        this.status = status;
        this.orderDate = orderDate;
        this.deadlineDate = deadlineDate;
        this.deliveryDate = deliveryDate;
        this.discount = 0;
        if(client.getRating() >= 75) {
            this.discount = 2;
        } else if(client.getRating() >= 150) {
            this.discount = 3;
        }
    }

    public ClientOrder(String identifier, Client client, OrderStatus status, Date orderDate, Date deadlineDate) {
        this.identifier = identifier;
        this.client = client;
        this.status = status;
        this.orderDate = orderDate;
        this.deadlineDate = deadlineDate;
        this.discount = 0;
        if(client.getRating() >= 75) {
            this.discount = 2;
        } else if(client.getRating() >= 150) {
            this.discount = 3;
        }
    }

    @Override
    public String toString() {
        return identifier;
    }
}
