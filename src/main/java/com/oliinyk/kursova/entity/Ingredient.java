package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Ingredient")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 25, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "unit_id", nullable = false)
    private Unit unit;

    @OneToMany(mappedBy = "ingredient")
    private List<SweetIngredient> sweets;

    @OneToMany(mappedBy = "ingredient")
    private List<StoredIngredient> storages;

    @OneToMany(mappedBy = "ingredient")
    private List<SupplyOrderContent> supplyOrderContents;

    public Ingredient(String name, Unit unit) {
        this.name = name;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return name;
    }
}
