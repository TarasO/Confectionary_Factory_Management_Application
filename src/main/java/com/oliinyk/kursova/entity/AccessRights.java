package com.oliinyk.kursova.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AccessRights")
public class AccessRights {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 30, nullable = false)
    private String name;

    @Column(length = 15, nullable = false)
    private String abbr;

    @OneToMany(mappedBy = "access")
    private List<User> users;
//
//    @Column(name = "is_deleted", nullable = false)
//    @ColumnDefault("false")
//    private boolean isDeleted;

    @Override
    public String toString() {
        return name;
    }
}
