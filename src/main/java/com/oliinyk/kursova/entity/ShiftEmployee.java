package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ShiftEmployee")
public class ShiftEmployee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(name = "hours_worked")
    private int hoursWorked;

    @ManyToOne
    @JoinColumn(name = "shift_id", nullable = false)
    private Shift shift;

    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    public ShiftEmployee(Shift shift, Employee employee, int hoursWorked) {
        this.shift = shift;
        this.employee = employee;
        this.hoursWorked = hoursWorked;
    }

    public ShiftEmployee(Employee employee, int hoursWorked) {
        this.employee = employee;
        this.hoursWorked = hoursWorked;
    }
}
