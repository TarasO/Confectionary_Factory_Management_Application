package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SupplyOrderContent")
public class SupplyOrderContent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @Column(precision = 10, scale = 2, nullable = false)
    private double price;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private SupplyOrder supplyOrder;

    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = false)
    private Ingredient ingredient;

    public SupplyOrderContent(SupplyOrder supplyOrder, Ingredient ingredient, double quantity, double price) {
        this.supplyOrder = supplyOrder;
        this.ingredient = ingredient;
        this.quantity = quantity;
        this.price = price;
    }

    public SupplyOrderContent(Ingredient ingredient, double quantity, double price) {
        this.ingredient = ingredient;
        this.quantity = quantity;
        this.price = price;
    }
}
