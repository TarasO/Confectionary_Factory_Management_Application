package com.oliinyk.kursova.entity;

import com.oliinyk.kursova.Utils;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Shift")
public class Shift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "shift_type_id", nullable = false)
    private ShiftType shiftType;

    @ManyToOne
    @JoinColumn(name = "workshop_id", nullable = false)
    private Workshop workshop;

    @OneToMany(mappedBy = "shift", fetch = FetchType.EAGER)
    private List<ShiftEmployee> employees;

    @OneToMany(mappedBy = "shift")
    private List<Production> productions;

    public Shift(ShiftType shiftType, Date date, Workshop workshop) {
        this.shiftType = shiftType;
        this.date = date;
        this.workshop = workshop;
    }

    @Override
    public String toString() {
        return shiftType.getName() + " " + Utils.formatDate(date);
    }
}
