package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ClientOrderContent")
public class ClientOrderContent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @Column(precision = 10, scale = 2, nullable = false)
    private double price;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private ClientOrder clientOrder;

    @ManyToOne
    @JoinColumn(name = "sweet_id", nullable = false)
    private Sweet sweet;

    public ClientOrderContent(ClientOrder clientOrder, Sweet sweet, double quantity, double price) {
        this.clientOrder = clientOrder;
        this.sweet = sweet;
        this.quantity = quantity;
        this.price = price;
    }

    public ClientOrderContent(Sweet sweet, double quantity, double price) {
        this.sweet = sweet;
        this.quantity = quantity;
        this.price = price;
    }
}
