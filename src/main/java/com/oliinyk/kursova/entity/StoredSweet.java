package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "StoredSweet")
public class StoredSweet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "sweet_id", nullable = false)
    private Sweet sweet;

    @ManyToOne
    @JoinColumn(name = "storage_id", nullable = false)
    private SweetStorage storage;

    public StoredSweet(SweetStorage storage, Sweet sweet, double quantity) {
        this.storage = storage;
        this.sweet = sweet;
        this.quantity = quantity;
    }
}
