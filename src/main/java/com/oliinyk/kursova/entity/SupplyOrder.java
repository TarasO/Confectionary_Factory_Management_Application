package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SupplyOrder")
public class SupplyOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 20, unique = true, nullable = false)
    private String identifier;

    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    @Column(name = "deadline_date", nullable = false)
    private Date deadlineDate;

    @Column(name = "delivery_date")
    private Date deliveryDate;

    @ManyToOne
    @JoinColumn(name = "supplier_id", nullable = false)
    private Supplier supplier;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private OrderStatus status;

    @OneToMany(mappedBy = "supplyOrder", fetch = FetchType.EAGER)
    private List<SupplyOrderContent> contents;

    public SupplyOrder(String identifier, Supplier supplier, OrderStatus status, Date orderDate, Date deadlineDate, Date deliveryDate) {
        this.identifier = identifier;
        this.supplier = supplier;
        this.status = status;
        this.orderDate = orderDate;
        this.deadlineDate = deadlineDate;
        this.deliveryDate = deliveryDate;
    }

    public SupplyOrder(String identifier, Supplier supplier, OrderStatus status, Date orderDate, Date deadlineDate) {
        this.identifier = identifier;
        this.supplier = supplier;
        this.status = status;
        this.orderDate = orderDate;
        this.deadlineDate = deadlineDate;
    }

    @Override
    public String toString() {
        return identifier;
    }
}
