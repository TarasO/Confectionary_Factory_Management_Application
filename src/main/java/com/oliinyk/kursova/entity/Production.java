package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Production")
public class Production {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 20, unique = true, nullable = false)
    private String identifier;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "sweet_id", nullable = false)
    private Sweet sweet;

    @ManyToOne
    @JoinColumn(name = "shift_id", nullable = false)
    private Shift shift;

    public Production(String identifier, Sweet sweet, Shift shift, double quantity) {
        this.identifier = identifier;
        this.sweet = sweet;
        this.shift = shift;
        this.quantity = quantity;
    }
}
