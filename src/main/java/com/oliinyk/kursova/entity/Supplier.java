package com.oliinyk.kursova.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Supplier")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 75)
    private String address;

    @Column(name = "phone_number", length = 75, nullable = false)
    private String phoneNumber;

    @Column
    @ColumnDefault("0")
    private int rating;

    @OneToMany(mappedBy = "supplier")
    private List<SupplyOrder> orders;

    public Supplier(String name, String address, String phoneNumber) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return name;
    }
}
