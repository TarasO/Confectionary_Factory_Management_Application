package com.oliinyk.kursova.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SweetIngredient")
public class SweetIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(precision = 7, scale = 2, nullable = false)
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "sweet_id", nullable = false)
    private Sweet sweet;

    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = false)
    private Ingredient ingredient;

    public SweetIngredient(Sweet sweet, Ingredient ingredient, double quantity) {
        this.sweet = sweet;
        this.ingredient = ingredient;
        this.quantity = quantity;
    }

    public SweetIngredient(Ingredient ingredient, double quantity) {
        this.ingredient = ingredient;
        this.quantity = quantity;
    }
}
