package com.oliinyk.kursova.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
@Table(name = "`User`")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(length = 20, nullable = false)
    private String username;

    @Column(length = 25, nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "access_id", nullable = false)
    private AccessRights access;

    public User(String username, String password, AccessRights access) {
        this.username = username;
        this.password = password;
        this.access = access;
    }
}
