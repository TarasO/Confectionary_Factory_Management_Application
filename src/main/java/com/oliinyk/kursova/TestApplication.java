package com.oliinyk.kursova;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import javafx.application.Application;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        Application.launch(JavaFXApplication.class, args);
    }

}
