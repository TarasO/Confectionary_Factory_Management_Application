package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {
}
