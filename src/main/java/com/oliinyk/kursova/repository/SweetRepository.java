package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Sweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SweetRepository extends JpaRepository<Sweet, Integer> {
}
