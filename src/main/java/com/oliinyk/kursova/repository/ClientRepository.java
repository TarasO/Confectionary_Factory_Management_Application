package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer> {
}
