package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.ClientOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientOrderRepository extends JpaRepository<ClientOrder, Integer> {
}
