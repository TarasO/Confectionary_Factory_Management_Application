package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.SupplyOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplyOrderRepository extends JpaRepository<SupplyOrder, Integer> {
}
