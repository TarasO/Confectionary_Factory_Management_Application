package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.AccessRights;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessRightsRepository extends JpaRepository<AccessRights, Integer> {
}
