package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
