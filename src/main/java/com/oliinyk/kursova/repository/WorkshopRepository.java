package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Workshop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkshopRepository extends JpaRepository<Workshop, Integer> {
}
