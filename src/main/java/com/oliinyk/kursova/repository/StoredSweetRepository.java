package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.StoredSweet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoredSweetRepository extends JpaRepository<StoredSweet, Integer> {
}
