package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.ShiftType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftTypeRepository extends JpaRepository<ShiftType, Integer> {
}
