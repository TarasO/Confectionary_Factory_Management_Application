package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Integer> {
}
