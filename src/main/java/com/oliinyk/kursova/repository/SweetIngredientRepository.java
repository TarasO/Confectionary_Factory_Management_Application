package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.SweetIngredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SweetIngredientRepository extends JpaRepository<SweetIngredient, Integer> {
}
