package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.StoredIngredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoredIngredientRepository extends JpaRepository<StoredIngredient, Integer> {
}
