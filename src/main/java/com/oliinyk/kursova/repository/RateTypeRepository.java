package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.RateType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateTypeRepository extends JpaRepository<RateType, Integer> {
}
