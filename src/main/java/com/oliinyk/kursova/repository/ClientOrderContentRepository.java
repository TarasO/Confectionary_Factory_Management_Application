package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.ClientOrderContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientOrderContentRepository extends JpaRepository<ClientOrderContent, Integer> {
}
