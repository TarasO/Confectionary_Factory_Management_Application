package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.ShiftEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftEmployeeRepository extends JpaRepository<ShiftEmployee, Integer> {
}
