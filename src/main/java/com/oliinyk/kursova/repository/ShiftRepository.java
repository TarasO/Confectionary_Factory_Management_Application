package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Shift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftRepository extends JpaRepository<Shift, Integer> {
}
