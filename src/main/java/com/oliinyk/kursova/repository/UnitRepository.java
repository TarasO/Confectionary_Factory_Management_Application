package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitRepository extends JpaRepository<Unit, Integer> {
}
