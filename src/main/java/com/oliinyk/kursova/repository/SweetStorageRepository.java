package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.SweetStorage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SweetStorageRepository extends JpaRepository<SweetStorage, Integer> {
}
