package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.IngredientStorage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientStorageRepository extends JpaRepository<IngredientStorage, Integer> {
}
