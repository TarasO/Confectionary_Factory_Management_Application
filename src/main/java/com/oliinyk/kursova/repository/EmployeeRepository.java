package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
