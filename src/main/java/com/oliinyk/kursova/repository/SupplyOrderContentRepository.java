package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.SupplyOrderContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplyOrderContentRepository extends JpaRepository<SupplyOrderContent, Integer> {
}
