package com.oliinyk.kursova.repository;

import com.oliinyk.kursova.entity.Production;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductionRepository extends JpaRepository<Production, Integer> {
}
