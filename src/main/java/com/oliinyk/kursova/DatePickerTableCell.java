package com.oliinyk.kursova;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WritableValue;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.sql.Date;
import java.time.LocalDate;

public class DatePickerTableCell<T> extends TableCell<T, Date> {

    private final DatePicker datePicker;
    private boolean listening = true;

    // listener for changes in the datepicker
    private final ChangeListener<LocalDate> listener = (observable, oldValue, newValue) -> {
        if (listening) {
            listening = false;

            TableColumn<T, Date> column = getTableColumn();
            EventHandler<TableColumn.CellEditEvent<T, Date>> handler = column.getOnEditCommit();
            if (handler != null) {
                // use TableColumn.onEditCommit if there is a handler
                handler.handle(new TableColumn.CellEditEvent<T, Date>(
                        (TableView<T>) getTableView(),
                        new TablePosition<T, Date>(getTableView(), getIndex(), column),
                        TableColumn.<T, Date>editCommitEvent(),
                        Date.valueOf(newValue)
                ));
            } else {
                // otherwise check if ObservableValue from cellValueFactory is
                // also writable and use in that case
                ObservableValue<Date> observableValue = column.getCellObservableValue((T) getTableRow().getItem());
                if (observableValue instanceof WritableValue) {
                    ((WritableValue) observableValue).setValue(newValue);
                }
            }

            listening = true;
        }
    };

    public DatePickerTableCell() {
        this.datePicker = new DatePicker();
        this.datePicker.valueProperty().addListener(listener);
    }

    @Override
    protected void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            listening = false;
            setGraphic(null);
        } else {
            listening = false;
            setGraphic(this.datePicker);
            if(item != null) {
                this.datePicker.setValue(item.toLocalDate());
            } else {
                this.datePicker.setValue(null);
            }
            listening = true;
        }
    }

    public static <E> Callback<TableColumn<E, Date>, TableCell<E, Date>> forTableColumn() {
        return column -> new DatePickerTableCell<>();
    }

}