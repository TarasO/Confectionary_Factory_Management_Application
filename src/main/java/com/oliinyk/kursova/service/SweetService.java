package com.oliinyk.kursova.service;

import com.oliinyk.kursova.entity.Sweet;
import com.oliinyk.kursova.entity.SweetIngredient;

import java.util.List;

public interface SweetService {
    Object[] save(Sweet sweet, List<SweetIngredient> ingredients);
}
