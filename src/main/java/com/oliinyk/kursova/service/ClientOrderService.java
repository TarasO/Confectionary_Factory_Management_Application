package com.oliinyk.kursova.service;

import com.oliinyk.kursova.entity.ClientOrder;
import com.oliinyk.kursova.entity.ClientOrderContent;

import java.util.List;

public interface ClientOrderService {
    ClientOrder save(ClientOrder clientOrder, List<ClientOrderContent> contents);
}
