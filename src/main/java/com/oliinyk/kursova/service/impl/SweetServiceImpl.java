package com.oliinyk.kursova.service.impl;

import com.oliinyk.kursova.entity.Sweet;
import com.oliinyk.kursova.entity.SweetIngredient;
import com.oliinyk.kursova.repository.SweetIngredientRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import com.oliinyk.kursova.service.SweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SweetServiceImpl implements SweetService {

    private final SweetRepository sweetRepository;
    private final SweetIngredientRepository sweetIngredientRepository;

    public SweetServiceImpl(SweetRepository sweetRepository, SweetIngredientRepository sweetIngredientRepository) {
        this.sweetRepository = sweetRepository;
        this.sweetIngredientRepository = sweetIngredientRepository;
    }

    @Transactional
    @Override
    public Object[] save(Sweet sweet, List<SweetIngredient> ingredients) {
        sweet = sweetRepository.save(sweet);
        Sweet finalSweet = sweet;
        ingredients.forEach(x -> {
            x.setSweet(finalSweet);
            x = sweetIngredientRepository.save(x);
        });

        return new Object[] {sweet, ingredients};
    }
}
