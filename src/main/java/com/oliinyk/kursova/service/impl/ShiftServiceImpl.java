package com.oliinyk.kursova.service.impl;

import com.oliinyk.kursova.entity.Shift;
import com.oliinyk.kursova.entity.ShiftEmployee;
import com.oliinyk.kursova.repository.ShiftEmployeeRepository;
import com.oliinyk.kursova.repository.ShiftRepository;
import com.oliinyk.kursova.service.ShiftService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShiftServiceImpl implements ShiftService {

    private final ShiftRepository shiftRepository;
    private final ShiftEmployeeRepository shiftEmployeeRepository;

    public ShiftServiceImpl(ShiftRepository shiftRepository, ShiftEmployeeRepository shiftEmployeeRepository) {
        this.shiftRepository = shiftRepository;
        this.shiftEmployeeRepository = shiftEmployeeRepository;
    }

    @Transactional
    @Override
    public Shift save(Shift shift, List<ShiftEmployee> shiftEmployees) {
        shift = shiftRepository.save(shift);
        Shift finalShift = shift;
        shiftEmployees.forEach(x -> {
            x.setShift(finalShift);
            x = shiftEmployeeRepository.save(x);
        });
        shift.setEmployees(shiftEmployees);

        return shift;
    }
}
