package com.oliinyk.kursova.service.impl;

import com.oliinyk.kursova.entity.ClientOrder;
import com.oliinyk.kursova.entity.ClientOrderContent;
import com.oliinyk.kursova.repository.ClientOrderContentRepository;
import com.oliinyk.kursova.repository.ClientOrderRepository;
import com.oliinyk.kursova.service.ClientOrderService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientOrderServiceImpl implements ClientOrderService {

    private final ClientOrderRepository clientOrderRepository;
    private final ClientOrderContentRepository clientOrderContentRepository;

    public ClientOrderServiceImpl(ClientOrderRepository clientOrderRepository, ClientOrderContentRepository clientOrderContentRepository) {
        this.clientOrderRepository = clientOrderRepository;
        this.clientOrderContentRepository = clientOrderContentRepository;
    }

    @Transactional
    @Override
    public ClientOrder save(ClientOrder clientOrder, List<ClientOrderContent> contents) {
        clientOrder = clientOrderRepository.save(clientOrder);
        ClientOrder finalClientOrder = clientOrder;
        contents.forEach(x -> {
            x.setClientOrder(finalClientOrder);
            x = clientOrderContentRepository.save(x);
        });
        clientOrder.setContents(contents);

        return clientOrder;
    }
}
