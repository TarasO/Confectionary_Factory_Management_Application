package com.oliinyk.kursova.service.impl;

import com.oliinyk.kursova.entity.SupplyOrder;
import com.oliinyk.kursova.entity.SupplyOrderContent;
import com.oliinyk.kursova.repository.SupplyOrderContentRepository;
import com.oliinyk.kursova.repository.SupplyOrderRepository;
import com.oliinyk.kursova.service.SupplyOrderService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SupplyOrderServiceImpl implements SupplyOrderService {

    private final SupplyOrderRepository supplyOrderRepository;
    private final SupplyOrderContentRepository supplyOrderContentRepository;

    public SupplyOrderServiceImpl(SupplyOrderRepository supplyOrderRepository, SupplyOrderContentRepository supplyOrderContentRepository) {
        this.supplyOrderRepository = supplyOrderRepository;
        this.supplyOrderContentRepository = supplyOrderContentRepository;
    }

    @Transactional
    @Override
    public SupplyOrder save(SupplyOrder supplyOrder, List<SupplyOrderContent> contents) {
        supplyOrder = supplyOrderRepository.save(supplyOrder);
        SupplyOrder finalSupplyOrder = supplyOrder;
        contents.forEach(x -> {
            x.setSupplyOrder(finalSupplyOrder);
            x = supplyOrderContentRepository.save(x);
        });

        supplyOrder.setContents(contents);

        return supplyOrder;
    }
}
