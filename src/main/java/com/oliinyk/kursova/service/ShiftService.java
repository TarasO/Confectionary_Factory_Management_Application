package com.oliinyk.kursova.service;

import com.oliinyk.kursova.entity.Shift;
import com.oliinyk.kursova.entity.ShiftEmployee;

import java.util.List;

public interface ShiftService {
    Shift save(Shift shift, List<ShiftEmployee> shiftEmployees);
}
