package com.oliinyk.kursova.service;

import com.oliinyk.kursova.entity.SupplyOrder;
import com.oliinyk.kursova.entity.SupplyOrderContent;

import java.util.List;

public interface SupplyOrderService {
    SupplyOrder save(SupplyOrder supplyOrder, List<SupplyOrderContent> contents);
}
