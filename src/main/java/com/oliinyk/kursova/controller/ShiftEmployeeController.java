package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.EmployeeRepository;
import com.oliinyk.kursova.repository.ShiftRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class ShiftEmployeeController {

    @FXML private ComboBox<Shift> insertShiftEmployeeShiftComboBox;
    @FXML private ComboBox<Employee> insertShiftEmployeeEmployeeComboBox;
    @FXML private TextField insertShiftEmployeeHoursWorked;
    @FXML private Button insertShiftEmployeeButton;

    private final ShiftRepository shiftRepository;
    private final EmployeeRepository employeeRepository;
    private final MainController mainController;

    public ShiftEmployeeController(ShiftRepository shiftRepository, EmployeeRepository employeeRepository, MainController mainController) {
        this.shiftRepository = shiftRepository;
        this.employeeRepository = employeeRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertShiftEmployeeShiftComboBox.setItems(FXCollections.observableList(shiftRepository.findAll()));
        insertShiftEmployeeEmployeeComboBox.setItems(FXCollections.observableList(employeeRepository.findAll()));

        insertShiftEmployeeButton.setOnAction(event -> {
            if(insertShiftEmployeeShiftComboBox.getSelectionModel().isEmpty() || insertShiftEmployeeEmployeeComboBox.getSelectionModel().isEmpty()
                    || insertShiftEmployeeHoursWorked.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                ShiftEmployee shiftEmployee = new ShiftEmployee(insertShiftEmployeeShiftComboBox.getValue(), insertShiftEmployeeEmployeeComboBox.getValue(),
                        Integer.parseInt(insertShiftEmployeeHoursWorked.getText()));
                mainController.addExternalItem(shiftEmployee);
            }
        });
    }
}
