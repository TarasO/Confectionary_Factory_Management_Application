package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.SupplyOrderRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import com.oliinyk.kursova.repository.SweetStorageRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class SupplyOrderContentController {

    @FXML private ComboBox<SupplyOrder> insertSupplyOrderContentOrderComboBox;
    @FXML private ComboBox<Ingredient> insertSupplyOrderContentIngredientComboBox;
    @FXML private TextField insertSupplyOrderContentQuantityField;
    @FXML private TextField insertSupplyOrderContentPriceField;
    @FXML private Button insertSupplyOrderContentButton;

    private final SupplyOrderRepository supplyOrderRepository;
    private final IngredientRepository ingredientRepository;

    private final MainController mainController;

    public SupplyOrderContentController(SupplyOrderRepository supplyOrderRepository, IngredientRepository ingredientRepository,
                                 MainController mainController) {
        this.supplyOrderRepository = supplyOrderRepository;
        this.ingredientRepository = ingredientRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertSupplyOrderContentOrderComboBox.setItems(FXCollections.observableList(supplyOrderRepository.findAll()));
        insertSupplyOrderContentIngredientComboBox.setItems(FXCollections.observableList(ingredientRepository.findAll()));

        insertSupplyOrderContentButton.setOnAction(event -> {
            if(insertSupplyOrderContentOrderComboBox.getSelectionModel().isEmpty()
                    || insertSupplyOrderContentIngredientComboBox.getSelectionModel().isEmpty()
                    || insertSupplyOrderContentQuantityField.getText().isBlank()
                    || insertSupplyOrderContentPriceField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                SupplyOrderContent supplyOrderContent = new SupplyOrderContent(insertSupplyOrderContentOrderComboBox.getValue(),
                        insertSupplyOrderContentIngredientComboBox.getValue(), Double.parseDouble(insertSupplyOrderContentQuantityField.getText()),
                        Double.parseDouble(insertSupplyOrderContentPriceField.getText()));
                mainController.addExternalItem(supplyOrderContent);
            }
        });
    }
}
