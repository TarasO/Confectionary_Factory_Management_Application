package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.IngredientStorage;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class IngredientStorageController {

    @FXML private TextField insertIngredientStorageNameField;
    @FXML private TextField insertIngredientStorageAddressField;
    @FXML private Button insertIngredientStorageButton;

    private final MainController mainController;

    public IngredientStorageController(MainController mainController) {
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertIngredientStorageButton.setOnAction(event -> {
            if(insertIngredientStorageNameField.getText().isBlank() || insertIngredientStorageAddressField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                IngredientStorage ingredientStorage = new IngredientStorage(insertIngredientStorageNameField.getText(),
                        insertIngredientStorageAddressField.getText());
                mainController.addExternalItem(ingredientStorage);
            }
        });
    }
}
