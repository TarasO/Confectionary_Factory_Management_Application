package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.ClientOrderRepository;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.SupplyOrderRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class ClientOrderContentController {

    @FXML private ComboBox<ClientOrder> insertClientOrderContentOrderComboBox;
    @FXML private ComboBox<Sweet> insertClientOrderContentSweetComboBox;
    @FXML private TextField insertClientOrderContentQuantityField;
    @FXML private TextField insertClientOrderContentPriceField;
    @FXML private Button insertClientOrderContentButton;

    private final ClientOrderRepository clientOrderRepository;
    private final SweetRepository ingredientRepository;

    private final MainController mainController;

    public ClientOrderContentController(ClientOrderRepository clientOrderRepository, SweetRepository ingredientRepository,
                                        MainController mainController) {
        this.clientOrderRepository = clientOrderRepository;
        this.ingredientRepository = ingredientRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertClientOrderContentOrderComboBox.setItems(FXCollections.observableList(clientOrderRepository.findAll()));
        insertClientOrderContentSweetComboBox.setItems(FXCollections.observableList(ingredientRepository.findAll()));

        insertClientOrderContentButton.setOnAction(event -> {
            if(insertClientOrderContentOrderComboBox.getSelectionModel().isEmpty()
                    || insertClientOrderContentSweetComboBox.getSelectionModel().isEmpty()
                    || insertClientOrderContentQuantityField.getText().isBlank()
                    || insertClientOrderContentPriceField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                ClientOrderContent clientOrderContent = new ClientOrderContent(insertClientOrderContentOrderComboBox.getValue(),
                        insertClientOrderContentSweetComboBox.getValue(), Double.parseDouble(insertClientOrderContentQuantityField.getText()),
                        Double.parseDouble(insertClientOrderContentPriceField.getText()));
                mainController.addExternalItem(clientOrderContent);
            }
        });
    }
}
