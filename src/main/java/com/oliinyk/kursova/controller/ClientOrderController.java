package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class ClientOrderController {

    @FXML private TextField insertClientOrderIdentifierField;
    @FXML private ComboBox<Client> insertClientOrderClientComboBox;
    @FXML private DatePicker insertClientOrderDatePicker;
    @FXML private DatePicker insertClientOrderDeadlineDatePicker;
    @FXML private Button insertClientOrderButton;

    @FXML private TableView<ClientOrderContent> insertClientOrderContentTable;
    @FXML private TableColumn<ClientOrderContent, Sweet> insertClientOrderContentSweetColumn;
    @FXML private TableColumn<ClientOrderContent, Double> insertClientOrderContentQuantityColumn;
    @FXML private TableColumn<ClientOrderContent, Double> insertClientOrderContentPriceColumn;

    @FXML private ComboBox<Sweet> insertClientOrderContentSweetComboBox;
    @FXML private TextField insertClientOrderContentQuantityField;
    @FXML private TextField insertClientOrderContentPriceField;
    @FXML private Button insertClientOrderContentButton;

    private final SweetRepository sweetRepository;
    private final ClientRepository clientRepository;
    private final OrderStatusRepository orderStatusRepository;

    private final MainController mainController;

    public ClientOrderController(SweetRepository sweetRepository, ClientRepository clientRepository,
                                 OrderStatusRepository orderStatusRepository, MainController mainController) {
        this.sweetRepository = sweetRepository;
        this.clientRepository = clientRepository;
        this.orderStatusRepository = orderStatusRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertClientOrderClientComboBox.setItems(FXCollections.observableList(clientRepository.findAll()));

        ObservableList<Sweet> sweetList = FXCollections.observableList(sweetRepository.findAll());
        insertClientOrderContentSweetComboBox.setItems(sweetList);

        //TableColumn
        insertClientOrderContentSweetColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSweet()));
        insertClientOrderContentSweetColumn.setCellFactory(ComboBoxTableCell.forTableColumn(sweetList));
        insertClientOrderContentSweetColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setSweet(cellEditEvent.getNewValue());
        });

        insertClientOrderContentQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        insertClientOrderContentQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        insertClientOrderContentQuantityColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setQuantity(cellEditEvent.getNewValue());
        });

        insertClientOrderContentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        insertClientOrderContentPriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        insertClientOrderContentPriceColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setPrice(cellEditEvent.getNewValue());
        });

        //Button
        insertClientOrderContentButton.setOnAction(event -> {
            if(insertClientOrderContentSweetComboBox.getSelectionModel().isEmpty()
                    || insertClientOrderContentQuantityField.getText().isEmpty() || insertClientOrderContentPriceField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                insertClientOrderContentTable.getItems().add(new ClientOrderContent(insertClientOrderContentSweetComboBox.getValue(),
                        Double.parseDouble(insertClientOrderContentQuantityField.getText()),
                        Double.parseDouble(insertClientOrderContentPriceField.getText())));
            }
        });

        insertClientOrderButton.setOnAction(event -> {
            if(insertClientOrderIdentifierField.getText().isBlank() || insertClientOrderClientComboBox.getSelectionModel().isEmpty()
                    || insertClientOrderDatePicker.getValue() == null || insertClientOrderDeadlineDatePicker.getValue() == null) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                ClientOrder supplyOrder = new ClientOrder(insertClientOrderIdentifierField.getText(),
                        insertClientOrderClientComboBox.getValue(), orderStatusRepository.findByName("Замовлено"),
                        Date.valueOf(insertClientOrderDatePicker.getValue()), Date.valueOf(insertClientOrderDeadlineDatePicker.getValue()));
                if(insertClientOrderContentTable.getItems().isEmpty()) {
                    mainController.addExternalItem(supplyOrder);
                } else {
                    mainController.addExternalItem(supplyOrder, insertClientOrderContentTable.getItems());
                }
            }
        });
    }
}
