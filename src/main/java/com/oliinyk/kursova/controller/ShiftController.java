package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.stream.Collectors;

@Component
public class ShiftController {

    @FXML private ComboBox<ShiftType> insertShiftTypeComboBox;
    @FXML private DatePicker insertShiftDatePicker;
    @FXML private ComboBox<Workshop> insertShiftWorkshopComboBox;
    @FXML private Button insertShiftButton;

    @FXML private TableView<ShiftEmployee> insertShiftEmployeeTable;
    @FXML private TableColumn<ShiftEmployee, Employee> insertShiftEmployeeNameColumn;
    @FXML private TableColumn<ShiftEmployee, Integer> insertShiftEmployeeHoursWorkedColumn;

    @FXML private ComboBox<Employee> insertShiftEmployeeEmployeeComboBox;
    @FXML private TextField insertShiftEmployeeHoursWorkedField;
    @FXML private Button insertShiftEmployeeButton;

    private final ShiftTypeRepository shiftTypeRepository;
    private final WorkshopRepository workshopRepository;
    private final EmployeeRepository employeeRepository;

    private final MainController mainController;

    public ShiftController(ShiftTypeRepository shiftTypeRepository, WorkshopRepository workshopRepository,
                           EmployeeRepository employeeRepository, MainController mainController) {
        this.shiftTypeRepository = shiftTypeRepository;
        this.workshopRepository = workshopRepository;
        this.employeeRepository = employeeRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertShiftTypeComboBox.setItems(FXCollections.observableList(shiftTypeRepository.findAll()));
        insertShiftWorkshopComboBox.setItems(FXCollections.observableList(workshopRepository.findAll()));

        ObservableList<Employee> employeeList = FXCollections.observableList(employeeRepository.findAll()
                .stream().filter(x -> x.getEndDate() == null).collect(Collectors.toList()));
        insertShiftEmployeeEmployeeComboBox.setItems(employeeList);

        //TableColumn
        insertShiftEmployeeNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getEmployee()));
        insertShiftEmployeeNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(employeeList));
        insertShiftEmployeeNameColumn.setOnEditCommit(cellEditEvent -> {
            ShiftEmployee shiftEmployee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shiftEmployee.setEmployee(cellEditEvent.getNewValue());
        });

        insertShiftEmployeeHoursWorkedColumn.setCellValueFactory(new PropertyValueFactory<>("hoursWorked"));
        insertShiftEmployeeHoursWorkedColumn.setOnEditCommit(cellEditEvent -> {
            ShiftEmployee shiftEmployee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shiftEmployee.setHoursWorked(cellEditEvent.getNewValue());
        });

        //Button
        insertShiftEmployeeButton.setOnAction(event -> {
            if(insertShiftEmployeeNameColumn.getText().isBlank() || insertShiftEmployeeHoursWorkedColumn.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                insertShiftEmployeeTable.getItems().add(new ShiftEmployee(insertShiftEmployeeEmployeeComboBox.getValue(),
                        Integer.parseInt(insertShiftEmployeeHoursWorkedField.getText())));
            }
        });

        insertShiftButton.setOnAction(event -> {
            if(insertShiftTypeComboBox.getSelectionModel().isEmpty()
                    || insertShiftDatePicker.getValue() == null
                    || insertShiftWorkshopComboBox.getSelectionModel().isEmpty()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Shift shift = new Shift(insertShiftTypeComboBox.getValue(), Date.valueOf(insertShiftDatePicker.getValue()),
                        insertShiftWorkshopComboBox.getValue());
                if(insertShiftEmployeeTable.getItems().isEmpty()) {
                    mainController.addExternalItem(shift);
                } else {
                    mainController.addExternalItem(shift, insertShiftEmployeeTable.getItems());
                }
            }
        });
    }
}
