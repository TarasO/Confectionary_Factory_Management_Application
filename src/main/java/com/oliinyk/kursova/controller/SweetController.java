package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.CategoryRepository;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.UnitRepository;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import org.springframework.stereotype.Component;

@Component
public class SweetController {

    @FXML private TextField insertSweetNameField;
    @FXML private ComboBox<Category> insertSweetCategoryComboBox;
    @FXML private ComboBox<Unit> insertSweetUnitComboBox;
    @FXML private Button insertSweetButton;

    @FXML private TableView<SweetIngredient> insertSweetIngredientTable;
    @FXML private TableColumn<SweetIngredient, Ingredient> insertSweetIngredientIngredientColumn;
    @FXML private TableColumn<SweetIngredient, Double> insertSweetIngredientQuantityColumn;
    @FXML private TableColumn<SweetIngredient, String> insertSweetIngredientUnitColumn;


    @FXML private ComboBox<Ingredient> insertSweetIngredientIngredientComboBox;
    @FXML private TextField insertSweetIngredientQuantityField;
    @FXML private Button insertSweetIngredientButton;

    private final CategoryRepository categoryRepository;
    private final UnitRepository unitRepository;
    private final IngredientRepository ingredientRepository;

    private final MainController mainController;

    public SweetController(CategoryRepository categoryRepository, UnitRepository unitRepository,
                                      IngredientRepository ingredientRepository, MainController mainController) {
        this.categoryRepository = categoryRepository;
        this.unitRepository = unitRepository;
        this.ingredientRepository = ingredientRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertSweetCategoryComboBox.setItems(FXCollections.observableList(categoryRepository.findAll()));
        insertSweetUnitComboBox.setItems(FXCollections.observableList(unitRepository.findAll()));

        ObservableList<Ingredient> ingredientList = FXCollections.observableList(ingredientRepository.findAll());
        insertSweetIngredientIngredientComboBox.setItems(ingredientList);

        //TableColumn
        insertSweetIngredientIngredientColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getIngredient()));
        insertSweetIngredientIngredientColumn.setCellFactory(ComboBoxTableCell.forTableColumn(ingredientList));
        insertSweetIngredientIngredientColumn.setOnEditCommit(cellEditEvent -> {
            SweetIngredient sweetIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetIngredient.setIngredient(cellEditEvent.getNewValue());
        });


        insertSweetIngredientQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        insertSweetIngredientQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        insertSweetIngredientQuantityColumn.setOnEditCommit(cellEditEvent -> {
            SweetIngredient sweetIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetIngredient.setQuantity(cellEditEvent.getNewValue());
        });


        insertSweetIngredientUnitColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIngredient().getUnit().getAbbr()));

        //Button
        insertSweetIngredientButton.setOnAction(event -> {
            if(insertSweetIngredientIngredientComboBox.getSelectionModel().isEmpty() || insertSweetIngredientQuantityColumn.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                insertSweetIngredientTable.getItems().add(new SweetIngredient(insertSweetIngredientIngredientComboBox.getValue(),
                                                                Double.parseDouble(insertSweetIngredientQuantityField.getText())));
            }
        });

        insertSweetButton.setOnAction(event -> {
            if(insertSweetButton.getText().isBlank()
                    || insertSweetCategoryComboBox.getSelectionModel().isEmpty()
                    || insertSweetUnitComboBox.getSelectionModel().isEmpty()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Sweet sweet = new Sweet(insertSweetNameField.getText(), insertSweetCategoryComboBox.getValue(),
                        insertSweetUnitComboBox.getValue());
                if(insertSweetIngredientTable.getItems().isEmpty()) {
                    mainController.addExternalItem(sweet);
                } else {
                    mainController.addExternalItem(sweet, insertSweetIngredientTable.getItems());
                }
            }
        });
    }
}
