package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.Ingredient;
import com.oliinyk.kursova.entity.Sweet;
import com.oliinyk.kursova.entity.SweetIngredient;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class SweetIngredientController {

    @FXML private ComboBox<Sweet> insertSweetIngredientSweetComboBox;
    @FXML private ComboBox<Ingredient> insertSweetIngredientIngredientComboBox;
    @FXML private TextField insertSweetIngredientQuantityField;
    @FXML private Button insertSweetIngredientButton;

    private final SweetRepository sweetRepository;
    private final IngredientRepository ingredientRepository;

    private final MainController mainController;

    public SweetIngredientController(SweetRepository sweetRepository, IngredientRepository ingredientRepository,
                                     MainController mainController) {
        this.sweetRepository = sweetRepository;
        this.ingredientRepository = ingredientRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertSweetIngredientSweetComboBox.setItems(FXCollections.observableList(sweetRepository.findAll()));
        insertSweetIngredientIngredientComboBox.setItems(FXCollections.observableList(ingredientRepository.findAll()));

        insertSweetIngredientButton.setOnAction(event -> {
            if(insertSweetIngredientSweetComboBox.getSelectionModel().isEmpty()
                    || insertSweetIngredientIngredientComboBox.getSelectionModel().isEmpty()
                    || insertSweetIngredientQuantityField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                SweetIngredient sweetIngredient = new SweetIngredient(insertSweetIngredientSweetComboBox.getValue(),
                        insertSweetIngredientIngredientComboBox.getValue(), Double.parseDouble(insertSweetIngredientQuantityField.getText()));
                mainController.addExternalItem(sweetIngredient);
            }
        });
    }
}
