package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.DatePickerTableCell;
import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.*;
import com.oliinyk.kursova.service.ClientOrderService;
import com.oliinyk.kursova.service.ShiftService;
import com.oliinyk.kursova.service.SupplyOrderService;
import com.oliinyk.kursova.service.SweetService;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MainController {

    private final DataSource dataSource;

    private final ConfigurableApplicationContext context;

    @FXML private Label userLabel;

    @FXML private TabPane tabPane;

    //UI - Tab
    @FXML private Tab userTab;
    @FXML private Tab categoryAndUnitTab;
    @FXML private Tab sweetTab;
    @FXML private Tab ingredientTab;
    @FXML private Tab orderStatusTab;
    @FXML private Tab storageIngredientTab;
    @FXML private Tab storageSweetTab;
    @FXML private Tab supplierTab;
    @FXML private Tab supplyOrderTab;
    @FXML private Tab clientTab;
    @FXML private Tab clientOrderTab;
    @FXML private Tab workshopProductionShiftTab;
    @FXML private Tab positionEmployeeTab;

    //UI - TableView

    //User
    @FXML private TableView<User> userTable;
    @FXML private TableColumn<User, String> userUsernameColumn;
    @FXML private TableColumn<User, String> userPasswordColumn;
    @FXML private TableColumn<User, AccessRights> userRightsColumn;

    //Category
    @FXML private TableView<Category> categoryTable;
    @FXML private TableColumn<Category, String> categoryNameColumn;

    //Unit
    @FXML private TableView<Unit> unitTable;
    @FXML private TableColumn<Unit, String> unitNameColumn;
    @FXML private TableColumn<Unit, String> unitAbbrColumn;

    //Sweet
    @FXML private TableView<Sweet> sweetTable;
    @FXML private TableColumn<Sweet, String> sweetNameColumn;
    @FXML private TableColumn<Sweet, Category> sweetCategoryColumn;
    @FXML private TableColumn<Sweet, Unit> sweetUnitColumn;

    //Sweet - Ingredient
    @FXML private TableView<SweetIngredient> sweetIngredientTable;
    @FXML private TableColumn<SweetIngredient, Ingredient> sweetIngredientNameColumn;
    @FXML private TableColumn<SweetIngredient, Double> sweetIngredientQuantityColumn;
    @FXML private TableColumn<SweetIngredient, String> sweetIngredientUnitColumn;

    //Ingredient
    @FXML private TableView<Ingredient> ingredientTable;
    @FXML private TableColumn<Ingredient, String> ingredientNameColumn;
    @FXML private TableColumn<Ingredient, Unit> ingredientUnitColumn;

    //OrderStatus
    @FXML private TableView<OrderStatus> orderStatusTable;
    @FXML private TableColumn<OrderStatus, String> orderStatusNameColumn;

    //IngredientStorage
    @FXML private TableView<IngredientStorage> ingredientStorageTable;
    @FXML private TableColumn<IngredientStorage, String> ingredientStorageNameColumn;
    @FXML private TableColumn<IngredientStorage, String> ingredientStorageAddressColumn;

    //StoredIngredient
    @FXML private TableView<StoredIngredient> storedIngredientTable;
    @FXML private TableColumn<StoredIngredient, Ingredient> storedIngredientNameColumn;
    @FXML private TableColumn<StoredIngredient, Double> storedIngredientQuantityColumn;
    @FXML private TableColumn<StoredIngredient, String> storedIngredientUnitColumn;

    //SweetStorage
    @FXML private TableView<SweetStorage> sweetStorageTable;
    @FXML private TableColumn<SweetStorage, String> sweetStorageNameColumn;
    @FXML private TableColumn<SweetStorage, String> sweetStorageAddressColumn;

    //StoredSweet
    @FXML private TableView<StoredSweet> storedSweetTable;
    @FXML private TableColumn<StoredSweet, Sweet> storedSweetNameColumn;
    @FXML private TableColumn<StoredSweet, Double> storedSweetQuantityColumn;
    @FXML private TableColumn<StoredSweet, String> storedSweetUnitColumn;

    //Supplier
    @FXML private TableView<Supplier> supplierTable;
    @FXML private TableColumn<Supplier, String> supplierNameColumn;
    @FXML private TableColumn<Supplier, String> supplierAddressColumn;
    @FXML private TableColumn<Supplier, String> supplierPhoneNumberColumn;
    @FXML private TableColumn<Supplier, Integer> supplierRatingColumn;

    //SupplyOrder
    @FXML private TableView<SupplyOrder> supplyOrderTable;
    @FXML private TableColumn<SupplyOrder, String> supplyOrderIdentifierColumn;
    @FXML private TableColumn<SupplyOrder, Supplier> supplyOrderSupplierColumn;
    @FXML private TableColumn<SupplyOrder, OrderStatus> supplyOrderStatusColumn;
    @FXML private TableColumn<SupplyOrder, Date> supplyOrderDateColumn;
    @FXML private TableColumn<SupplyOrder, Date> supplyOrderDeadlineDateColumn;
    @FXML private TableColumn<SupplyOrder, Date> supplyOrderDeliveryDateColumn;

    //SupplyOrderContentOrder
    @FXML private TableView<SupplyOrder> supplyOrderContentOrderTable;
    @FXML private TableColumn<SupplyOrder, String> supplyOrderContentOrderIdentifierColumn;
    @FXML private TableColumn<SupplyOrder, Supplier> supplyOrderContentOrderSupplierColumn;

    //SupplyOrderContent
    @FXML private TableView<SupplyOrderContent> supplyOrderContentTable;
    @FXML private TableColumn<SupplyOrderContent, Ingredient> supplyOrderContentIngredientColumn;
    @FXML private TableColumn<SupplyOrderContent, Double> supplyOrderContentQuantityColumn;
    @FXML private TableColumn<SupplyOrderContent, Double> supplyOrderContentPriceColumn;

    //Client
    @FXML private TableView<Client> clientTable;
    @FXML private TableColumn<Client, String> clientNameColumn;
    @FXML private TableColumn<Client, String> clientAddressColumn;
    @FXML private TableColumn<Client, String> clientPhoneNumberColumn;
    @FXML private TableColumn<Client, Integer> clientRatingColumn;

    //ClientOrder
    @FXML private TableView<ClientOrder> clientOrderTable;
    @FXML private TableColumn<ClientOrder, String> clientOrderIdentifierColumn;
    @FXML private TableColumn<ClientOrder, Client> clientOrderClientColumn;
    @FXML private TableColumn<ClientOrder, OrderStatus> clientOrderStatusColumn;
    @FXML private TableColumn<ClientOrder, Date> clientOrderDateColumn;
    @FXML private TableColumn<ClientOrder, Date> clientOrderDeadlineDateColumn;
    @FXML private TableColumn<ClientOrder, Date> clientOrderDeliveryDateColumn;
    @FXML private TableColumn<ClientOrder, Integer> clientOrderDiscountColumn;

    //ClientOrderContentOrder
    @FXML private TableView<ClientOrder> clientOrderContentOrderTable;
    @FXML private TableColumn<ClientOrder, String> clientOrderContentOrderIdentifierColumn;
    @FXML private TableColumn<ClientOrder, Client> clientOrderContentOrderClientColumn;

    //ClientOrderContent
    @FXML private TableView<ClientOrderContent> clientOrderContentTable;
    @FXML private TableColumn<ClientOrderContent, Sweet> clientOrderContentSweetColumn;
    @FXML private TableColumn<ClientOrderContent, Double> clientOrderContentQuantityColumn;
    @FXML private TableColumn<ClientOrderContent, Double> clientOrderContentPriceColumn;

    //Workshop
    @FXML private TableView<Workshop> workshopTable;
    @FXML private TableColumn<Workshop, String> workshopNameColumn;
    @FXML private TableColumn<Workshop, String> workshopAddressColumn;

    //Production
    @FXML private TableView<Production> productionTable;
    @FXML private TableColumn<Production, String> productionIdentifierColumn;
    @FXML private TableColumn<Production, Sweet> productionSweetColumn;
    @FXML private TableColumn<Production, Shift> productionShiftColumn;
    @FXML private TableColumn<Production, Workshop> productionWorkshopColumn;
    @FXML private TableColumn<Production, Double> productionQuantityColumn;

    //ShiftType
    @FXML private TableView<ShiftType> shiftTypeTable;
    @FXML private TableColumn<ShiftType, String> shiftTypeNameColumn;

    //Shift
    @FXML private TableView<Shift> shiftTable;
    @FXML private TableColumn<Shift, ShiftType> shiftTypeColumn;
    @FXML private TableColumn<Shift, Date> shiftDateColumn;
    @FXML private TableColumn<Shift, Workshop> shiftWorkshopColumn;

    //ShiftEmployee
    @FXML private TableView<ShiftEmployee> shiftEmployeeTable;
    @FXML private TableColumn<ShiftEmployee, Employee> shiftEmployeeNameColumn;
    @FXML private TableColumn<ShiftEmployee, Integer> shiftEmployeeHoursColumn;

    //RateType
    @FXML private TableView<RateType> rateTypeTable;
    @FXML private TableColumn<RateType, String> rateTypeNameColumn;

    //Position
    @FXML private TableView<Position> positionTable;
    @FXML private TableColumn<Position, String> positionNameColumn;
    @FXML private TableColumn<Position, String> positionAbbrColumn;
    @FXML private TableColumn<Position, RateType> positionRateTypeColumn;
    @FXML private TableColumn<Position, Double> positionPaymentColumn;
    @FXML private TableColumn<Position, Integer> positionNightBonusColumn;
    @FXML private TableColumn<Position, Integer> positionLoyaltyBonusColumn;

    //Employee
    @FXML private TableView<Employee> employeeTable;
    @FXML private TableColumn<Employee, String> employeeLastNameColumn;
    @FXML private TableColumn<Employee, String> employeeFirstNameColumn;
    @FXML private TableColumn<Employee, Date> employeeBirthDateColumn;
    @FXML private TableColumn<Employee, Position> employeePositionColumn;
    @FXML private TableColumn<Employee, Workshop> employeeWorkshopColumn;
    @FXML private TableColumn<Employee, String> employeePhoneNumberColumn;
    @FXML private TableColumn<Employee, Date> employeeBeginDateColumn;
    @FXML private TableColumn<Employee, Date> employeeEndDateColumn;

    //UI - Insert

    //User
    @FXML private TextField insertUserNameField;
    @FXML private TextField insertUserPasswordField;
    @FXML private ComboBox<AccessRights> insertUserRightsComboBox;
    @FXML private Button insertUserButton;

    //Category
    @FXML private TextField insertCategoryNameField;
    @FXML private Button insertCategoryButton;

    //Unit
    @FXML private TextField insertUnitNameField;
    @FXML private TextField insertUnitAbbrField;
    @FXML private Button insertUnitButton;

    //Sweet
    @FXML private Button insertSweetButton;

    //SweetIngredient
    @FXML private Button insertSweetIngredientButton;

    //Ingredient
    @FXML private TextField insertIngredientNameField;
    @FXML private ComboBox<Unit> insertIngredientUnitComboBox;
    @FXML private Button insertIngredientButton;

    //IngredientStorage
    @FXML private Button insertIngredientStorageButton;

    //StoredIngredient
    @FXML private Button insertStoredIngredientButton;

    //SweetStorage
    @FXML private Button insertSweetStorageButton;

    //StoredSweet
    @FXML private Button insertStoredSweetButton;

    //Supplier
    @FXML private TextField insertSupplierNameField;
    @FXML private TextField insertSupplierAddressField;
    @FXML private TextField insertSupplierPhoneNumberField;
    @FXML private Button insertSupplierButton;

    //SupplyOrder
    @FXML private Button insertSupplyOrderButton;

    //SupplyOrderContent
    @FXML private Button insertSupplyOrderContentButton;

    //Client
    @FXML private TextField insertClientNameField;
    @FXML private TextField insertClientAddressField;
    @FXML private TextField insertClientPhoneNumberField;
    @FXML private Button insertClientButton;

    //ClientOrder
    @FXML private Button insertClientOrderButton;

    //ClientOrderContent
    @FXML private Button insertClientOrderContentButton;

    //Workshop
    @FXML private TextField insertWorkshopNameField;
    @FXML private TextField insertWorkshopAddressField;
    @FXML private Button insertWorkshopButton;

    //Production
    @FXML private Button insertProductionButton;

    //Shift
    @FXML private Button insertShiftButton;

    //ShiftEmployee
    @FXML private Button insertShiftEmployeeButton;

    //Position
    @FXML private Button insertPositionButton;

    //Employee
    @FXML private Button insertEmployeeButton;

    //UI - Delete

    //User
    @FXML private Button deleteUserButton;

    //SweetIngredient
    @FXML private Button deleteSweetIngredientButton;

    //StoredIngredient
    @FXML private Button deleteStoredIngredientButton;

    //StoredSweet
    @FXML private Button deleteStoredSweetButton;

    //SupplyOrderContent
    @FXML private Button deleteSupplyOrderContentButton;

    //ClientOrderContent
    @FXML private Button deleteClientOrderContentButton;

    //Production
    @FXML private Button deleteProductionButton;

    //ShiftEmployee
    @FXML private Button deleteShiftEmployeeButton;

    //UI - Report
    @FXML private Button supplyOrderReportButton;

    @FXML private Button clientOrderReportButton;

    //UI - System
    @FXML private Button refreshDataButton;

    @FXML private Button applyUpdatesButton;

    //Repository
    private final AccessRightsRepository accessRightsRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final UnitRepository unitRepository;
    private final SweetRepository sweetRepository;
    private final SweetIngredientRepository sweetIngredientRepository;
    private final IngredientRepository ingredientRepository;
    private final OrderStatusRepository orderStatusRepository;
    private final IngredientStorageRepository ingredientStorageRepository;
    private final StoredIngredientRepository storedIngredientRepository;
    private final SweetStorageRepository sweetStorageRepository;
    private final StoredSweetRepository storedSweetRepository;
    private final SupplierRepository supplierRepository;
    private final SupplyOrderRepository supplyOrderRepository;
    private final SupplyOrderContentRepository supplyOrderContentRepository;
    private final ClientRepository clientRepository;
    private final ClientOrderRepository clientOrderRepository;
    private final ClientOrderContentRepository clientOrderContentRepository;
    private final WorkshopRepository workshopRepository;
    private final ProductionRepository productionRepository;
    private final ShiftTypeRepository shiftTypeRepository;
    private final ShiftRepository shiftRepository;
    private final ShiftEmployeeRepository shiftEmployeeRepository;
    private final RateTypeRepository rateTypeRepository;
    private final PositionRepository positionRepository;
    private final EmployeeRepository employeeRepository;

    //Service
    private final SweetService sweetService;
    private final SupplyOrderService supplyOrderService;
    private final ClientOrderService clientOrderService;
    private final ShiftService shiftService;

    public MainController(DataSource dataSource,
                          ConfigurableApplicationContext context,
                          AccessRightsRepository accessRightsRepository,UserRepository userRepository,
                          CategoryRepository categoryRepository, UnitRepository unitRepository, SweetRepository sweetRepository,
                          IngredientRepository ingredientRepository, OrderStatusRepository orderStatusRepository,
                          SupplyOrderContentRepository supplyOrderContentRepository, ClientRepository clientRepository,
                          SweetIngredientRepository sweetIngredientRepository, IngredientStorageRepository ingredientStorageRepository,
                          StoredIngredientRepository storedIngredientRepository, SweetStorageRepository sweetStorageRepository,
                          StoredSweetRepository storedSweetRepository, ClientOrderRepository clientOrderRepository,
                          SupplierRepository supplierRepository, SupplyOrderRepository supplyOrderRepository,
                          ClientOrderContentRepository clientOrderContentRepository, ProductionRepository productionRepository,
                          WorkshopRepository workshopRepository, ShiftTypeRepository shiftTypeRepository, ShiftRepository shiftRepository,
                          ShiftEmployeeRepository shiftEmployeeRepository, RateTypeRepository rateTypeRepository,
                          PositionRepository positionRepository, EmployeeRepository employeeRepository,
                          SweetService sweetService, SupplyOrderService supplyOrderService, ClientOrderService clientOrderService,
                          ShiftService shiftService) {
        this.dataSource = dataSource;
        this.context = context;
        this.accessRightsRepository = accessRightsRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.unitRepository = unitRepository;
        this.sweetRepository = sweetRepository;
        this.sweetIngredientRepository = sweetIngredientRepository;
        this.ingredientRepository = ingredientRepository;
        this.orderStatusRepository = orderStatusRepository;
        this.supplyOrderContentRepository = supplyOrderContentRepository;
        this.clientRepository = clientRepository;
        this.ingredientStorageRepository = ingredientStorageRepository;
        this.storedIngredientRepository = storedIngredientRepository;
        this.sweetStorageRepository = sweetStorageRepository;
        this.storedSweetRepository = storedSweetRepository;
        this.clientOrderRepository = clientOrderRepository;
        this.supplierRepository = supplierRepository;
        this.supplyOrderRepository = supplyOrderRepository;
        this.clientOrderContentRepository = clientOrderContentRepository;
        this.workshopRepository = workshopRepository;
        this.productionRepository = productionRepository;
        this.shiftTypeRepository = shiftTypeRepository;
        this.shiftRepository = shiftRepository;
        this.shiftEmployeeRepository = shiftEmployeeRepository;
        this.rateTypeRepository = rateTypeRepository;
        this.positionRepository = positionRepository;
        this.employeeRepository = employeeRepository;

        this.sweetService = sweetService;
        this.supplyOrderService = supplyOrderService;
        this.clientOrderService = clientOrderService;
        this.shiftService = shiftService;
    }

    //User
    private User user;

    //Lists
    private final ObservableList<AccessRights> accessRightsList = FXCollections.observableArrayList();
    private final ObservableList<User> userList = FXCollections.observableArrayList();
    private final ObservableList<Unit> unitList = FXCollections.observableArrayList();
    private final ObservableList<Category> categoryList = FXCollections.observableArrayList();
    private final ObservableList<Sweet> sweetList = FXCollections.observableArrayList();
    private final ObservableList<SweetIngredient> sweetIngredientList = FXCollections.observableArrayList();
    private final ObservableList<Ingredient> ingredientList = FXCollections.observableArrayList();
    private final ObservableList<OrderStatus> orderStatusList = FXCollections.observableArrayList();
    private final ObservableList<IngredientStorage> ingredientStorageList = FXCollections.observableArrayList();
    private final ObservableList<SweetStorage> sweetStorageList = FXCollections.observableArrayList();
    private final ObservableList<Supplier> supplierList = FXCollections.observableArrayList();
    private final ObservableList<SupplyOrder> supplyOrderList = FXCollections.observableArrayList();
    private final ObservableList<Client> clientList = FXCollections.observableArrayList();
    private final ObservableList<ClientOrder> clientOrderList = FXCollections.observableArrayList();
    private final ObservableList<Workshop> workshopList = FXCollections.observableArrayList();
    private final ObservableList<Production> productionList = FXCollections.observableArrayList();
    private final ObservableList<ShiftType> shiftTypeList = FXCollections.observableArrayList();
    private final ObservableList<Shift> shiftList = FXCollections.observableArrayList();
    private final ObservableList<RateType> rateTypeList = FXCollections.observableArrayList();
    private final ObservableList<Position> positionList = FXCollections.observableArrayList();
    private final ObservableList<Employee> employeeList = FXCollections.observableArrayList();

    //UpdateSet
    private final Set<User> userUpdateSet = new HashSet<>();
    private final Set<Category> categoryUpdateSet = new HashSet<>();
    private final Set<Unit> unitUpdateSet = new HashSet<>();
    private final Set<Sweet> sweetUpdateSet = new HashSet<>();
    private final Set<SweetIngredient> sweetIngredientUpdateSet = new HashSet<>();
    private final Set<Ingredient> ingredientUpdateSet = new HashSet<>();
    private final Set<IngredientStorage> ingredientStorageUpdateSet = new HashSet<>();
    private final Set<StoredIngredient> storedIngredientUpdateSet = new HashSet<>();
    private final Set<SweetStorage> sweetStorageUpdateSet = new HashSet<>();
    private final Set<StoredSweet> storedSweetUpdateSet = new HashSet<>();
    private final Set<Supplier> supplierUpdateSet = new HashSet<>();
    private final Set<SupplyOrder> supplyOrderUpdateSet = new HashSet<>();
    private final Set<SupplyOrderContent> supplyOrderContentUpdateSet = new HashSet<>();
    private final Set<Client> clientUpdateSet = new HashSet<>();
    private final Set<ClientOrder> clientOrderUpdateSet = new HashSet<>();
    private final Set<ClientOrderContent> clientOrderContentUpdateSet = new HashSet<>();
    private final Set<Workshop> workshopUpdateSet = new HashSet<>();
    private final Set<Production> productionUpdateSet = new HashSet<>();
    private final Set<Shift> shiftUpdateSet = new HashSet<>();
    private final Set<ShiftEmployee> shiftEmployeeUpdateSet = new HashSet<>();
    private final Set<Position> positionUpdateSet = new HashSet<>();
    private final Set<Employee> employeeUpdateSet = new HashSet<>();

    @FXML
    private void initialize() {

        //UI - TableView
        setTableViewItems();

        //User

        //User - Username
        userUsernameColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        userUsernameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userUsernameColumn.setOnEditCommit(cellEditEvent -> {
            User user = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            user.setUsername(cellEditEvent.getNewValue());
            userUpdateSet.add(user);
        });

        //User - Password
        userPasswordColumn.setCellValueFactory(new PropertyValueFactory<>("password"));
        userPasswordColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userPasswordColumn.setOnEditCommit(cellEditEvent -> {
            User user = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            user.setPassword(cellEditEvent.getNewValue());
            userUpdateSet.add(user);
        });

        //User - AccessRights
        userRightsColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getAccess()));
        userRightsColumn.setCellFactory(ComboBoxTableCell.forTableColumn(accessRightsList));
        userRightsColumn.setOnEditCommit(cellEditEvent -> {
            User user = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            user.setAccess(cellEditEvent.getNewValue());
            userUpdateSet.add(user);
        });

        //Category

        //Category - Name
        categoryNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        categoryNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        categoryNameColumn.setOnEditCommit(cellEditEvent -> {
            Category category = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            category.setName(cellEditEvent.getNewValue());
            categoryUpdateSet.add(category);
        });

        //Unit

        //Unit - Name
        unitNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        unitNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        unitNameColumn.setOnEditCommit(cellEditEvent -> {
            Unit unit = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            unit.setName(cellEditEvent.getNewValue());
            unitUpdateSet.add(unit);
        });

        //Unit - Abbr
        unitAbbrColumn.setCellValueFactory(new PropertyValueFactory<>("abbr"));
        unitAbbrColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        unitAbbrColumn.setOnEditCommit(cellEditEvent -> {
            Unit unit = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            unit.setAbbr(cellEditEvent.getNewValue());
            unitUpdateSet.add(unit);
        });

        //Sweet

        //Sweet - Name
        sweetNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        sweetNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        sweetNameColumn.setOnEditCommit(cellEditEvent -> {
            Sweet sweet = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweet.setName(cellEditEvent.getNewValue());
            sweetUpdateSet.add(sweet);
        });

        //Sweet - Category
        sweetCategoryColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getCategory()));
        sweetCategoryColumn.setCellFactory(ComboBoxTableCell.forTableColumn(categoryList));
        sweetCategoryColumn.setOnEditCommit(cellEditEvent -> {
            Sweet sweet = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweet.setCategory(cellEditEvent.getNewValue());
            sweetUpdateSet.add(sweet);
        });

        //Sweet - Unit
        sweetUnitColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getUnit()));
        sweetUnitColumn.setCellFactory(ComboBoxTableCell.forTableColumn(unitList));
        sweetUnitColumn.setOnEditCommit(cellEditEvent -> {
            Sweet sweet = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweet.setUnit(cellEditEvent.getNewValue());
            sweetUpdateSet.add(sweet);
        });

        //Sweet * Selection
        sweetTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
//                sweetIngredientTable.setItems(FXCollections.observableList(newSelection.getIngredients()));
                sweetIngredientTable.setItems(FXCollections.observableList(sweetIngredientList.stream().filter(x -> x.getSweet().getId() == newSelection.getId()).collect(Collectors.toList())));
            }
        });

        //SweetIngredient

        //SweetIngredient - Name
        sweetIngredientNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getIngredient()));
        sweetIngredientNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(ingredientList));
        sweetIngredientNameColumn.setOnEditCommit(cellEditEvent -> {
            SweetIngredient sweetIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetIngredient.setIngredient(cellEditEvent.getNewValue());
            sweetIngredientUpdateSet.add(sweetIngredient);
        });

        //SweetIngredient - Quantity
        sweetIngredientQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        sweetIngredientQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        sweetIngredientQuantityColumn.setOnEditCommit(cellEditEvent -> {
            SweetIngredient sweetIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetIngredient.setQuantity(cellEditEvent.getNewValue());
            sweetIngredientUpdateSet.add(sweetIngredient);
        });

        //SweetIngredient - Unit
        sweetIngredientUnitColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIngredient().getUnit().getAbbr()));

        //Ingredient

        //Ingredient - Name
        ingredientNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        ingredientNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        ingredientNameColumn.setOnEditCommit(cellEditEvent -> {
            Ingredient ingredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            ingredient.setName(cellEditEvent.getNewValue());
            ingredientUpdateSet.add(ingredient);
        });

        //Ingredient - Unit
        ingredientUnitColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getUnit()));
        ingredientUnitColumn.setCellFactory(ComboBoxTableCell.forTableColumn(unitList));
        ingredientUnitColumn.setOnEditCommit(cellEditEvent -> {
            Ingredient ingredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            ingredient.setUnit(cellEditEvent.getNewValue());
            ingredientUpdateSet.add(ingredient);
        });

        //OrderStatus

        //OrderStatus - Name
        orderStatusNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        orderStatusNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        orderStatusNameColumn.setOnEditCommit(cellEditEvent -> {
            OrderStatus orderStatus = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            orderStatus.setName(cellEditEvent.getNewValue());
        });

        //IngredientStorage

        //IngredientStorage - Name
        ingredientStorageNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        ingredientStorageNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        ingredientStorageNameColumn.setOnEditCommit(cellEditEvent -> {
            IngredientStorage ingredientStorage = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            ingredientStorage.setName(cellEditEvent.getNewValue());
            ingredientStorageUpdateSet.add(ingredientStorage);
        });

        //IngredientStorage - Address
        ingredientStorageAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        ingredientStorageAddressColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        ingredientStorageAddressColumn.setOnEditCommit(cellEditEvent -> {
            IngredientStorage ingredientStorage = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            ingredientStorage.setAddress(cellEditEvent.getNewValue());
            ingredientStorageUpdateSet.add(ingredientStorage);
        });

        //IngredientStorage * Selection
        ingredientStorageTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                if(newSelection.getIngredients() != null) {
                    storedIngredientTable.setItems(FXCollections.observableList(newSelection.getIngredients()));
                } else {
                    storedIngredientTable.setItems(FXCollections.emptyObservableList());
                }
            }
        });

        //StoredIngredient

        //StoredIngredient - Name
        storedIngredientNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getIngredient()));
        storedIngredientNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(ingredientList));
        storedIngredientNameColumn.setOnEditCommit(cellEditEvent -> {
            StoredIngredient storedIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            storedIngredient.setIngredient(cellEditEvent.getNewValue());
            storedIngredientUpdateSet.add(storedIngredient);
        });

        //StoredIngredient - Quantity
        storedIngredientQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        storedIngredientQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        storedIngredientQuantityColumn.setOnEditCommit(cellEditEvent -> {
            StoredIngredient storedIngredient = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            storedIngredient.setQuantity(cellEditEvent.getNewValue());
            storedIngredientUpdateSet.add(storedIngredient);
        });

        //StoredIngredient - Unit
        storedIngredientUnitColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIngredient().getUnit().getAbbr()));

        //SweetStorage

        //SweetStorage - Name
        sweetStorageNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        sweetStorageNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        sweetStorageNameColumn.setOnEditCommit(cellEditEvent -> {
            SweetStorage sweetStorage = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetStorage.setName(cellEditEvent.getNewValue());
            sweetStorageUpdateSet.add(sweetStorage);
        });

        //SweetStorage - Address
        sweetStorageAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        sweetStorageAddressColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        sweetStorageAddressColumn.setOnEditCommit(cellEditEvent -> {
            SweetStorage sweetStorage = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            sweetStorage.setAddress(cellEditEvent.getNewValue());
            sweetStorageUpdateSet.add(sweetStorage);
        });

        //SweetStorage * Selection
        sweetStorageTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                if(newSelection.getSweets() != null) {
                    storedSweetTable.setItems(FXCollections.observableList(newSelection.getSweets()));
                } else {
                    storedSweetTable.setItems(FXCollections.emptyObservableList());
                }
            }
        });

        //StoredSweet

        //StoredSweet - Name
        storedSweetNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSweet()));
        storedSweetNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(sweetList));
        storedSweetNameColumn.setOnEditCommit(cellEditEvent -> {
            StoredSweet storedSweet = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            storedSweet.setSweet(cellEditEvent.getNewValue());
            storedSweetUpdateSet.add(storedSweet);
        });

        //StoredSweet - Quantity
        storedSweetQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        storedSweetQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        storedSweetQuantityColumn.setOnEditCommit(cellEditEvent -> {
            StoredSweet storedSweet = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            storedSweet.setQuantity(cellEditEvent.getNewValue());
            storedSweetUpdateSet.add(storedSweet);
        });

        //StoredSweet - Unit
        storedSweetUnitColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSweet().getUnit().getAbbr()));

        //Supplier

        //Supplier - Name
        supplierNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        supplierNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        supplierNameColumn.setOnEditCommit(cellEditEvent -> {
            Supplier supplier = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplier.setName(cellEditEvent.getNewValue());
            supplierUpdateSet.add(supplier);
        });

        //Supplier - Address
        supplierAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        supplierAddressColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        supplierAddressColumn.setOnEditCommit(cellEditEvent -> {
            Supplier supplier = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplier.setAddress(cellEditEvent.getNewValue());
            supplierUpdateSet.add(supplier);
        });

        //Supplier - Phone Number
        supplierPhoneNumberColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        supplierPhoneNumberColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        supplierPhoneNumberColumn.setOnEditCommit(cellEditEvent -> {
            Supplier supplier = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplier.setPhoneNumber(cellEditEvent.getNewValue());
            supplierUpdateSet.add(supplier);
        });

        //Supplier - Rating
        supplierRatingColumn.setCellValueFactory(new PropertyValueFactory<>("rating"));
        supplierRatingColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        supplierRatingColumn.setOnEditCommit(cellEditEvent -> {
            Supplier supplier = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplier.setRating(cellEditEvent.getNewValue());
            supplierUpdateSet.add(supplier);
        });

        //SupplyOrder

        //SupplyOrder - Identifier
        supplyOrderIdentifierColumn.setCellValueFactory(new PropertyValueFactory<>("identifier"));
        supplyOrderIdentifierColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        supplyOrderIdentifierColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setIdentifier(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrder - Supplier
        supplyOrderSupplierColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSupplier()));
        supplyOrderSupplierColumn.setCellFactory(ComboBoxTableCell.forTableColumn(supplierList));
        supplyOrderSupplierColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setSupplier(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrder - Status
        supplyOrderStatusColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getStatus()));
        supplyOrderStatusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(orderStatusList));
        supplyOrderStatusColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setStatus(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrder - OrderDate
        supplyOrderDateColumn.setCellValueFactory(new PropertyValueFactory<>("orderDate"));
        if(supplyOrderTable.isEditable()) {
            supplyOrderDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            supplyOrderDateColumn.setCellFactory(dateCustomCellFactory());
        }
        supplyOrderDateColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setOrderDate(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrder - DeadlineDate
        supplyOrderDeadlineDateColumn.setCellValueFactory(new PropertyValueFactory<>("deadlineDate"));
        if(supplyOrderTable.isEditable()) {
            supplyOrderDeadlineDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            supplyOrderDeadlineDateColumn.setCellFactory(dateCustomCellFactory());
        }
        supplyOrderDeadlineDateColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setDeadlineDate(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrder - DeliveryDate
        supplyOrderDeliveryDateColumn.setCellValueFactory(new PropertyValueFactory<>("deliveryDate"));
        if(supplyOrderTable.isEditable()) {
            supplyOrderDeliveryDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            supplyOrderDeliveryDateColumn.setCellFactory(dateCustomCellFactory());
        }
        supplyOrderDeliveryDateColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrder supplyOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrder.setDeliveryDate(cellEditEvent.getNewValue());
            supplyOrderUpdateSet.add(supplyOrder);
        });

        //SupplyOrderContentOrder

        //SupplyOrderContentOrder - Identifier
        supplyOrderContentOrderIdentifierColumn.setCellValueFactory(new PropertyValueFactory<>("identifier"));

        //SupplyOrderContentOrder - Supplier
        supplyOrderContentOrderSupplierColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSupplier()));

        //SupplyOrderContentOrder * Selection
        supplyOrderContentOrderTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                if(newSelection.getContents() != null) {
                    supplyOrderContentTable.setItems(FXCollections.observableList(newSelection.getContents()));
                } else {
                    supplyOrderContentTable.setItems(FXCollections.emptyObservableList());
                }
            }
        });

        //SupplyOrderContent

        //SupplyOrderContent - Ingredient
        supplyOrderContentIngredientColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getIngredient()));
        supplyOrderContentIngredientColumn.setCellFactory(ComboBoxTableCell.forTableColumn(ingredientList));
        supplyOrderContentIngredientColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setIngredient(cellEditEvent.getNewValue());
            supplyOrderContentUpdateSet.add(supplyOrderContent);
        });

        //SupplyOrderContent - Quantity
        supplyOrderContentQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        supplyOrderContentQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        supplyOrderContentQuantityColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setQuantity(cellEditEvent.getNewValue());
            supplyOrderContentUpdateSet.add(supplyOrderContent);
        });

        //SupplyOrderContent - Price
        supplyOrderContentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        supplyOrderContentPriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        supplyOrderContentPriceColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setPrice(cellEditEvent.getNewValue());
            supplyOrderContentUpdateSet.add(supplyOrderContent);
        });

        //Client

        //Client - Name
        clientNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        clientNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        clientNameColumn.setOnEditCommit(cellEditEvent -> {
            Client client = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            client.setName(cellEditEvent.getNewValue());
            clientUpdateSet.add(client);
        });

        //Client - Address
        clientAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        clientAddressColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        clientAddressColumn.setOnEditCommit(cellEditEvent -> {
            Client client = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            client.setAddress(cellEditEvent.getNewValue());
            clientUpdateSet.add(client);
        });

        //Client - Phone Number
        clientPhoneNumberColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        clientPhoneNumberColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        clientPhoneNumberColumn.setOnEditCommit(cellEditEvent -> {
            Client client = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            client.setPhoneNumber(cellEditEvent.getNewValue());
            clientUpdateSet.add(client);
        });

        //Client - Rating
        clientRatingColumn.setCellValueFactory(new PropertyValueFactory<>("rating"));
        clientRatingColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        clientRatingColumn.setOnEditCommit(cellEditEvent -> {
            Client client = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            client.setRating(cellEditEvent.getNewValue());
            clientUpdateSet.add(client);
        });

        //ClientOrder

        //ClientOrder - Identifier
        clientOrderIdentifierColumn.setCellValueFactory(new PropertyValueFactory<>("identifier"));
        clientOrderIdentifierColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        clientOrderIdentifierColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setIdentifier(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - Client
        clientOrderClientColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getClient()));
        clientOrderClientColumn.setCellFactory(ComboBoxTableCell.forTableColumn(clientList));
        clientOrderClientColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setClient(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - Status
        clientOrderStatusColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getStatus()));
        clientOrderStatusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(orderStatusList));
        clientOrderStatusColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setStatus(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - OrderDate
        clientOrderDateColumn.setCellValueFactory(new PropertyValueFactory<>("orderDate"));
        if(clientOrderTable.isEditable()) {
            clientOrderDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            clientOrderDateColumn.setCellFactory(dateCustomCellFactory());
        }
        clientOrderDateColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setOrderDate(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - DeadlineDate
        clientOrderDeadlineDateColumn.setCellValueFactory(new PropertyValueFactory<>("deadlineDate"));
        if(clientOrderTable.isEditable()) {
            clientOrderDeadlineDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            clientOrderDeadlineDateColumn.setCellFactory(dateCustomCellFactory());
        }
        clientOrderDeadlineDateColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setDeadlineDate(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - DeliveryDate
        clientOrderDeliveryDateColumn.setCellValueFactory(new PropertyValueFactory<>("deliveryDate"));
        if(clientOrderTable.isEditable()) {
            clientOrderDeliveryDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            clientOrderDeliveryDateColumn.setCellFactory(dateCustomCellFactory());
        }
        clientOrderDeliveryDateColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setDeliveryDate(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrder - Discount
        clientOrderDiscountColumn.setCellValueFactory(new PropertyValueFactory<>("discount"));
        clientOrderDiscountColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        clientOrderDiscountColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrder clientOrder = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrder.setDiscount(cellEditEvent.getNewValue());
            clientOrderUpdateSet.add(clientOrder);
        });

        //ClientOrderContentOrder

        //ClientOrderContentOrder - Identifier
        clientOrderContentOrderIdentifierColumn.setCellValueFactory(new PropertyValueFactory<>("identifier"));

        //ClientOrderContentOrder - Client
        clientOrderContentOrderClientColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getClient()));

        //ClientOrderContentOrder * Selection
        clientOrderContentOrderTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                clientOrderContentTable.setItems(FXCollections.observableList(newSelection.getContents()));
            }
        });

        //ClientOrderContent

        //ClientOrderContent - Sweet
        clientOrderContentSweetColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSweet()));
        clientOrderContentSweetColumn.setCellFactory(ComboBoxTableCell.forTableColumn(sweetList));
        clientOrderContentSweetColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent clientOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrderContent.setSweet(cellEditEvent.getNewValue());
            clientOrderContentUpdateSet.add(clientOrderContent);
        });

        //ClientOrderContent - Quantity
        clientOrderContentQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        clientOrderContentQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        clientOrderContentQuantityColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent clientOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrderContent.setQuantity(cellEditEvent.getNewValue());
            clientOrderContentUpdateSet.add(clientOrderContent);
        });

        //SupplyOrderContent - Price
        clientOrderContentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        clientOrderContentPriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        clientOrderContentPriceColumn.setOnEditCommit(cellEditEvent -> {
            ClientOrderContent clientOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            clientOrderContent.setPrice(cellEditEvent.getNewValue());
            clientOrderContentUpdateSet.add(clientOrderContent);
        });

        //Workshop

        //Workshop - Name
        workshopNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        workshopNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        workshopNameColumn.setOnEditCommit(cellEditEvent -> {
            Workshop workshop = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            workshop.setName(cellEditEvent.getNewValue());
            workshopUpdateSet.add(workshop);
        });

        //Workshop - Address
        workshopAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        workshopAddressColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        workshopAddressColumn.setOnEditCommit(cellEditEvent -> {
            Workshop workshop = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            workshop.setAddress(cellEditEvent.getNewValue());
            workshopUpdateSet.add(workshop);
        });

        //Production

        //Production - Identifier
        productionIdentifierColumn.setCellValueFactory(new PropertyValueFactory<>("identifier"));
        productionIdentifierColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        productionIdentifierColumn.setOnEditCommit(cellEditEvent -> {
            Production production = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            production.setIdentifier(cellEditEvent.getNewValue());
            productionUpdateSet.add(production);
        });

        //Production - Sweet
        productionSweetColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSweet()));
        productionSweetColumn.setCellFactory(ComboBoxTableCell.forTableColumn(sweetList));
        productionSweetColumn.setOnEditCommit(cellEditEvent -> {
            Production production = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            production.setSweet(cellEditEvent.getNewValue());
            productionUpdateSet.add(production);
        });

        //Production - Shift
        productionShiftColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getShift()));
        productionShiftColumn.setCellFactory(ComboBoxTableCell.forTableColumn(shiftList));
        productionShiftColumn.setOnEditCommit(cellEditEvent -> {
            Production production = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            production.setShift(cellEditEvent.getNewValue());
            productionUpdateSet.add(production);
        });

        //Production - Workshop
        productionWorkshopColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getShift().getWorkshop()));

        //Production - Quantity
        productionQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        productionQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        productionQuantityColumn.setOnEditCommit(cellEditEvent -> {
            Production production = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            production.setQuantity(cellEditEvent.getNewValue());
            productionUpdateSet.add(production);
        });

        //ShiftType

        //ShiftType - Name
        shiftTypeNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        //Shift

        //Shift - Type
        shiftTypeColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getShiftType()));
        shiftTypeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(shiftTypeList));
        shiftTypeColumn.setOnEditCommit(cellEditEvent -> {
            Shift shift = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shift.setShiftType(cellEditEvent.getNewValue());
            shiftUpdateSet.add(shift);
        });

        //Shift - Date
        shiftDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        if(shiftTable.isEditable()) {
            shiftDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            shiftDateColumn.setCellFactory(dateCustomCellFactory());
        }
        shiftDateColumn.setOnEditCommit(cellEditEvent -> {
            Shift shift = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shift.setDate(cellEditEvent.getNewValue());
            shiftUpdateSet.add(shift);
        });

        //Shift - Workshop
        shiftWorkshopColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getWorkshop()));
        shiftWorkshopColumn.setCellFactory(ComboBoxTableCell.forTableColumn(workshopList));
        shiftWorkshopColumn.setOnEditCommit(cellEditEvent -> {
            Shift shift = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shift.setWorkshop(cellEditEvent.getNewValue());
            shiftUpdateSet.add(shift);
        });

        //Shift * Selection
        shiftTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                //shiftEmployeeNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(FXCollections.observableList(employeeList)));
                //.stream().filter(x -> x.getWorkshop().getId() == newSelection.getWorkshop().getId()).collect(Collectors.toList()));
                shiftEmployeeTable.setItems(FXCollections.observableList(newSelection.getEmployees()));
            }
        });

        //ShiftEmployee

        //ShiftEmployee - Employee
        shiftEmployeeNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getEmployee()));
        shiftEmployeeNameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(employeeList));
        shiftEmployeeNameColumn.setOnEditCommit(cellEditEvent -> {
            ShiftEmployee shiftEmployee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shiftEmployee.setEmployee(cellEditEvent.getNewValue());
            shiftEmployeeUpdateSet.add(shiftEmployee);
        });

        //ShiftEmployee - Hours Worked
        shiftEmployeeHoursColumn.setCellValueFactory(new PropertyValueFactory<>("hoursWorked"));
        shiftEmployeeHoursColumn.setOnEditCommit(cellEditEvent -> {
            ShiftEmployee shiftEmployee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            shiftEmployee.setHoursWorked(cellEditEvent.getNewValue());
            shiftEmployeeUpdateSet.add(shiftEmployee);
        });

        //RateType

        //RateType - Name
        rateTypeNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        //Position

        //Position - Name
        positionNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        positionNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        positionNameColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setName(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Position - Abbr
        positionAbbrColumn.setCellValueFactory(new PropertyValueFactory<>("abbr"));
        positionAbbrColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        positionAbbrColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setAbbr(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Position - RateType
        positionRateTypeColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getRateType()));
        positionRateTypeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(rateTypeList));
        positionRateTypeColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setRateType(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Position - Payment
        positionPaymentColumn.setCellValueFactory(new PropertyValueFactory<>("payment"));
        positionPaymentColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        positionPaymentColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setPayment(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Position - NightShiftBonus
        positionNightBonusColumn.setCellValueFactory(new PropertyValueFactory<>("nightShiftBonus"));
        positionNightBonusColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        positionNightBonusColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setNightShiftBonus(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Position - Loyalty
        positionLoyaltyBonusColumn.setCellValueFactory(new PropertyValueFactory<>("loyaltyBonus"));
        positionLoyaltyBonusColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        positionLoyaltyBonusColumn.setOnEditCommit(cellEditEvent -> {
            Position position = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            position.setLoyaltyBonus(cellEditEvent.getNewValue());
            positionUpdateSet.add(position);
        });

        //Employee

        //Employee - Last Name
        employeeLastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        employeeLastNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        employeeLastNameColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setLastName(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - First Name
        employeeFirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        employeeFirstNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        employeeFirstNameColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setFirstName(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - Birth Date
        employeeBirthDateColumn.setCellValueFactory(new PropertyValueFactory<>("birthDate"));
        if(employeeTable.isEditable()) {
            employeeBirthDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            employeeBirthDateColumn.setCellFactory(dateCustomCellFactory());
        }
        employeeBirthDateColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setBirthDate(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - Position
        employeePositionColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getPosition()));
        employeePositionColumn.setCellFactory(ComboBoxTableCell.forTableColumn(positionList));
        employeePositionColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setPosition(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - Workshop
        employeeWorkshopColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getWorkshop()));
        employeeWorkshopColumn.setCellFactory(ComboBoxTableCell.forTableColumn(workshopList));
        employeeWorkshopColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setWorkshop(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - Phone Number
        employeePhoneNumberColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        employeePhoneNumberColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        employeePhoneNumberColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setPhoneNumber(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - Begin Date
        employeeBeginDateColumn.setCellValueFactory(new PropertyValueFactory<>("beginDate"));
        if(employeeTable.isEditable()) {
            employeeBeginDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            employeeBeginDateColumn.setCellFactory(dateCustomCellFactory());
        }
        employeeBeginDateColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setBeginDate(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //Employee - End Date
        employeeEndDateColumn.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        if(employeeTable.isEditable()) {
            employeeEndDateColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        } else {
            employeeEndDateColumn.setCellFactory(dateCustomCellFactory());
        }
        employeeEndDateColumn.setOnEditCommit(cellEditEvent -> {
            Employee employee = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            employee.setEndDate(cellEditEvent.getNewValue());
            employeeUpdateSet.add(employee);
        });

        //UI - Insert

        //User

        //User - Rights ComboBox
        insertUserRightsComboBox.setItems(accessRightsList);

        //User - Add Button
        insertUserButton.setOnAction(event -> {
            if(insertUserNameField.getText().isBlank() || insertUserPasswordField.getText().isBlank() || insertUserRightsComboBox.getSelectionModel().isEmpty()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                User user = new User(insertUserNameField.getText(), insertUserPasswordField.getText(), insertUserRightsComboBox.getValue());
                user = userRepository.save(user);

                userList.add(user);
            }
        });

        //Category

        //Category - Add Button
        insertCategoryButton.setOnAction(event -> {
            if(insertCategoryNameField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Category category = new Category(insertCategoryNameField.getText());
                category = categoryRepository.save(category);

                categoryList.add(category);
            }
        });

        //Unit

        //Unit - Add Button
        insertUnitButton.setOnAction(event -> {
            if(insertUnitNameField.getText().isBlank() || insertUnitAbbrField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Unit unit = new Unit(insertUnitNameField.getText(), insertUnitAbbrField.getText());
                unit = unitRepository.save(unit);

                unitList.add(unit);
            }
        });

        //Sweet
        insertSweetButton.setOnAction(event -> openAddWindow("/fxml/sweet.fxml"));

        //SweetIngredient

        //SweetIngredient - Add Button
        insertSweetIngredientButton.setOnAction(event -> openAddWindow("/fxml/sweetIngredient.fxml"));

        //Ingredient

        //Ingredient - Unit ComboBox
        insertIngredientUnitComboBox.setItems(unitList);

        //Ingredient - Add Button
        insertIngredientButton.setOnAction(event -> {
            if(insertIngredientNameField.getText().isBlank()  || insertIngredientUnitComboBox.getSelectionModel().isEmpty()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Ingredient ingredient = new Ingredient(insertIngredientNameField.getText(), insertIngredientUnitComboBox.getValue());
                ingredient = ingredientRepository.save(ingredient);

                ingredientList.add(ingredient);
            }
        });

        //IngredientStorage

        //IngredientStorage - AddButton
        insertIngredientStorageButton.setOnAction(event -> openAddWindow("/fxml/ingredientStorage.fxml"));

        //StoredIngredient

        //StoredIngredient - Add Button
        insertStoredIngredientButton.setOnAction(event -> openAddWindow("/fxml/storedIngredient.fxml"));

        //SweetStorage

        //SweetStorage - Add Button
        insertSweetStorageButton.setOnAction(event -> openAddWindow("/fxml/sweetStorage.fxml"));

        //StoredSweet

        //StoredSweet - Add Button
        insertStoredSweetButton.setOnAction(event -> openAddWindow("/fxml/storedSweet.fxml"));

        //Supplier

        //Supplier - Add Button
        insertSupplierButton.setOnAction(event -> {
            if(insertSupplierNameField.getText().isBlank() || insertSupplierAddressField.getText().isBlank() || insertSupplierPhoneNumberField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Supplier supplier = new Supplier(insertSupplierNameField.getText(), insertSupplierAddressField.getText(), insertSupplierPhoneNumberField.getText());
                supplier = supplierRepository.save(supplier);

                supplierList.add(supplier);
            }
        });

        //SupplyOrder

        //SupplyOrder - Add Button
        insertSupplyOrderButton.setOnAction(event -> openAddWindow("/fxml/supplyOrder.fxml"));

        //SupplyOrderContent

        //SupplyOrderContent - Add Button
        insertSupplyOrderContentButton.setOnAction(event -> openAddWindow("/fxml/supplyOrderContent.fxml"));

        //Client

        //Client - Add Button
        insertClientButton.setOnAction(event -> {
            if(insertClientNameField.getText().isBlank() || insertClientAddressField.getText().isBlank() || insertClientPhoneNumberField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Client client = new Client(insertClientNameField.getText(), insertClientAddressField.getText(), insertClientPhoneNumberField.getText());
                client = clientRepository.save(client);

                clientList.add(client);
            }
        });

        //ClientOrder

        //ClientOrderContent - Add Button
        insertClientOrderButton.setOnAction(event -> openAddWindow("/fxml/clientOrder.fxml"));

        //ClientOrderContent

        //ClientOrderContent - Add Button
        insertClientOrderContentButton.setOnAction(event -> openAddWindow("/fxml/clientOrderContent.fxml"));

        //Workshop

        //Workshop - Add Button
        insertWorkshopButton.setOnAction(event -> {
            if(insertWorkshopNameField.getText().isBlank() || insertWorkshopAddressField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Workshop workshop = new Workshop(insertWorkshopNameField.getText(), insertWorkshopAddressField.getText());
                workshop = workshopRepository.save(workshop);

                workshopList.add(workshop);
            }
        });

        //Production

        //Production - Add Button
        insertProductionButton.setOnAction(event -> openAddWindow("/fxml/production.fxml"));

        //Shift

        //Shift - Add Button
        insertShiftButton.setOnAction(event -> openAddWindow("/fxml/shift.fxml"));

        //ShiftEmployee

        //ShiftEmployee - Add Button
        insertShiftEmployeeButton.setOnAction(event -> openAddWindow("/fxml/shiftEmployee.fxml"));

        //Position

        //Position - Add Button
        insertPositionButton.setOnAction(event -> openAddWindow("/fxml/position.fxml"));

        //Employee

        //Employee - Add Button
        insertEmployeeButton.setOnAction(event -> openAddWindow("/fxml/employee.fxml"));

        //UI - Delete

        //User

        //User - Delete Button
        deleteUserButton.setOnAction(event -> {
            try {
                User user = userTable.getSelectionModel().getSelectedItem();
                if(this.user.equals(user)){
                    Utils.showAlert("Помилка", "Неможливо видалити себе", Alert.AlertType.ERROR);
                } else {
                    userRepository.delete(user);
                    userList.remove(user);
                }
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //SweetIngredient

        //SweetIngredient - Delete Button
        deleteSweetIngredientButton.setOnAction(event -> {
            try {
                SweetIngredient sweetIngredient = sweetIngredientTable.getSelectionModel().getSelectedItem();
                sweetIngredientRepository.delete(sweetIngredient);
                sweetIngredientList.remove(sweetIngredient);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //StoredIngredient

        //StoredIngredient - Delete Button
        deleteStoredIngredientButton.setOnAction(event -> {
            try {
                StoredIngredient storedIngredient = storedIngredientTable.getSelectionModel().getSelectedItem();
                storedIngredientRepository.delete(storedIngredient);
                IngredientStorage ingredientStorage = ingredientStorageList.stream().filter(x -> x.getId() == storedIngredient.getStorage().getId()).findFirst().get();
                ingredientStorage.getIngredients().remove(storedIngredient);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //StoredSweet

        //StoredSweet - Delete Button
        deleteStoredSweetButton.setOnAction(event -> {
            try {
                StoredSweet storedSweet = storedSweetTable.getSelectionModel().getSelectedItem();
                storedSweetRepository.delete(storedSweet);
                SweetStorage sweetStorage = sweetStorageList.stream().filter(x -> x.getId() == storedSweet.getStorage().getId()).findFirst().get();
                sweetStorage.getSweets().remove(storedSweet);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //SupplyOrderContent

        //SupplyOrderContent - DeleteButton
        deleteSupplyOrderContentButton.setOnAction(event -> {
            try {
                SupplyOrderContent supplyOrderContent = supplyOrderContentTable.getSelectionModel().getSelectedItem();
                supplyOrderContentRepository.delete(supplyOrderContent);
                SupplyOrder supplyOrder = supplyOrderList.stream().filter(x -> x.getId() == supplyOrderContent.getSupplyOrder().getId()).findFirst().get();
                supplyOrder.getContents().remove(supplyOrderContent);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //ClientOrderContent

        //ClientOrderContent - DeleteButton
        deleteClientOrderContentButton.setOnAction(event -> {
            try {
                ClientOrderContent clientOrderContent = clientOrderContentTable.getSelectionModel().getSelectedItem();
                clientOrderContentRepository.delete(clientOrderContent);
                ClientOrder clientOrder = clientOrderList.stream().filter(x -> x.getId() == clientOrderContent.getClientOrder().getId()).findFirst().get();
                clientOrder.getContents().remove(clientOrderContent);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //Production

        //Production - Delete Button
        deleteProductionButton.setOnAction(event -> {
            try {
                Production production = productionTable.getSelectionModel().getSelectedItem();
                productionRepository.delete(production);
                productionList.remove(production);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //ShiftEmployee

        //ShiftEmployee - Delete Button
        deleteShiftEmployeeButton.setOnAction(event -> {
            try {
                ShiftEmployee shiftEmployee = shiftEmployeeTable.getSelectionModel().getSelectedItem();
                shiftEmployeeRepository.delete(shiftEmployee);
                Shift shift = shiftList.stream().filter(x -> x.getId() == shiftEmployee.getShift().getId()).findFirst().get();
                shift.getEmployees().remove(shiftEmployee);
            } catch(Exception e) {
                Utils.showAlert("Помилка", "Помилка видалення даних", Alert.AlertType.ERROR);
            }
        });

        //UI - Report
        supplyOrderReportButton.setOnAction(event -> {
            if(!supplyOrderTable.getSelectionModel().isEmpty()) {
                try {
                    JasperReport jasperReport = JasperCompileManager.compileReport(getClass().getResourceAsStream("/jrxml/supplyOrder.jrxml"));
                    int id = supplyOrderTable.getSelectionModel().getSelectedItem().getId();
                    Map<String, Object> parameters = new HashMap<>();
                    parameters.put("selected_order_id", id);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource.getConnection());

                    JasperExportManager.exportReportToHtmlFile(jasperPrint, "supplyOrder" + id + ".html");
                } catch (JRException | SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        clientOrderReportButton.setOnAction(event -> {
            if(!clientOrderTable.getSelectionModel().isEmpty()) {
                try {
                    JasperReport jasperReport = JasperCompileManager.compileReport(getClass().getResourceAsStream("/jrxml/clientOrder.jrxml"));
                    int id = clientOrderTable.getSelectionModel().getSelectedItem().getId();
                    Map<String, Object> parameters = new HashMap<>();
                    parameters.put("selected_order_id", id);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource.getConnection());

                    JasperExportManager.exportReportToHtmlFile(jasperPrint, "clientOrder" + id + ".html");
                } catch (JRException | SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        //UI - System
        refreshDataButton.setOnAction(event -> refreshData());

        applyUpdatesButton.setOnAction(event -> {
            applyUpdates(userUpdateSet, userRepository);
            applyUpdates(categoryUpdateSet, categoryRepository);
            applyUpdates(unitUpdateSet, unitRepository);
            applyUpdates(sweetUpdateSet, sweetRepository);
            applyUpdates(sweetIngredientUpdateSet, sweetIngredientRepository);
            applyUpdates(ingredientUpdateSet, ingredientRepository);
            applyUpdates(ingredientStorageUpdateSet, ingredientStorageRepository);
            applyUpdates(storedIngredientUpdateSet, storedIngredientRepository);
            applyUpdates(sweetStorageUpdateSet, sweetStorageRepository);
            applyUpdates(storedSweetUpdateSet, storedSweetRepository);
            applyUpdates(supplierUpdateSet, supplierRepository);
            applyUpdates(supplyOrderUpdateSet, supplyOrderRepository);
            applyUpdates(supplyOrderContentUpdateSet, supplyOrderContentRepository);
            applyUpdates(clientUpdateSet, clientRepository);
            applyUpdates(clientOrderUpdateSet, clientOrderRepository);
            applyUpdates(clientOrderContentUpdateSet, clientOrderContentRepository);
            applyUpdates(workshopUpdateSet, workshopRepository);
            applyUpdates(productionUpdateSet, productionRepository);
            applyUpdates(shiftUpdateSet, shiftRepository);
            applyUpdates(shiftEmployeeUpdateSet, shiftEmployeeRepository);
            applyUpdates(positionUpdateSet, positionRepository);
            applyUpdates(employeeUpdateSet, employeeRepository);
        });
    }

    public void setUser(User user) {
        this.user = user;
        userLabel.setText("Користувач: " + user.getUsername() + "(" + user.getAccess().getName() + ")");
    }

    private void refreshData() {
        clearDataLists();
        applyDataRetrievingAccessLimitations();
    }

    private void clearDataLists() {
        accessRightsList.clear();
        userList.clear();
        categoryList.clear();
        unitList.clear();
        sweetList.clear();
        sweetIngredientList.clear();
        ingredientList.clear();
        orderStatusList.clear();
        ingredientStorageList.clear();
        sweetStorageList.clear();
        supplierList.clear();
        supplyOrderList.clear();
        clientList.clear();
        clientOrderList.clear();
        workshopList.clear();
        productionList.clear();
        shiftTypeList.clear();
        shiftList.clear();
        rateTypeList.clear();
        positionList.clear();
        employeeList.clear();
    }

    public void applyDataRetrievingAccessLimitations() {
        if(user.getAccess().getAbbr().equals("admin")) {
            accessRightsList.addAll(accessRightsRepository.findAll());
            userList.addAll(userRepository.findAll());
            categoryList.addAll(categoryRepository.findAll());
            unitList.addAll(unitRepository.findAll());
            sweetList.addAll(sweetRepository.findAll());
            sweetIngredientList.addAll(sweetIngredientRepository.findAll());
            ingredientList.addAll(ingredientRepository.findAll());
            orderStatusList.addAll(orderStatusRepository.findAll());
            ingredientStorageList.addAll(ingredientStorageRepository.findAll());
            sweetStorageList.addAll(sweetStorageRepository.findAll());
            supplierList.addAll(supplierRepository.findAll());
            supplyOrderList.addAll(supplyOrderRepository.findAll());
            clientList.addAll(clientRepository.findAll());
            clientOrderList.addAll(clientOrderRepository.findAll());
            workshopList.addAll(workshopRepository.findAll());
            productionList.addAll(productionRepository.findAll());
            shiftTypeList.addAll(shiftTypeRepository.findAll());
            shiftList.addAll(shiftRepository.findAll());
            rateTypeList.addAll(rateTypeRepository.findAll());
            positionList.addAll(positionRepository.findAll());
            employeeList.addAll(employeeRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("chief")) {
            sweetList.addAll(sweetRepository.findAll());
            sweetIngredientList.addAll(sweetIngredientRepository.findAll());
            ingredientList.addAll(ingredientRepository.findAll());
            clientOrderList.addAll(clientOrderRepository.findAll());
            workshopList.addAll(workshopRepository.findAll());
            productionList.addAll(productionRepository.findAll());
            shiftTypeList.addAll(shiftTypeRepository.findAll());
            shiftList.addAll(shiftRepository.findAll());
            rateTypeList.addAll(rateTypeRepository.findAll());
            positionList.addAll(positionRepository.findAll());
            employeeList.addAll(employeeRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("sale")) {
            sweetList.addAll(sweetRepository.findAll());
            sweetIngredientList.addAll(sweetIngredientRepository.findAll());
            clientList.addAll(clientRepository.findAll());
            clientOrderList.addAll(clientOrderRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("purchase")) {
            ingredientList.addAll(ingredientRepository.findAll());
            ingredientStorageList.addAll(ingredientStorageRepository.findAll());
            supplierList.addAll(supplierRepository.findAll());
            supplyOrderList.addAll(supplyOrderRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("storage")) {
            sweetList.addAll(sweetRepository.findAll());
            ingredientList.addAll(ingredientRepository.findAll());
            ingredientStorageList.addAll(ingredientStorageRepository.findAll());
            sweetStorageList.addAll(sweetStorageRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("sweet")) {
            categoryList.addAll(categoryRepository.findAll());
            unitList.addAll(unitRepository.findAll());
            sweetList.addAll(sweetRepository.findAll());
            sweetIngredientList.addAll(sweetIngredientRepository.findAll());
            ingredientList.addAll(ingredientRepository.findAll());
        } else if(user.getAccess().getAbbr().equals("staff")) {
            rateTypeList.addAll(rateTypeRepository.findAll());
            positionList.addAll(positionRepository.findAll());
            employeeList.addAll(employeeRepository.findAll());
        }
    }

    public void applyDataAccessLimitations() {
        if(user.getAccess().getAbbr().equals("chief")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(categoryAndUnitTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(storageIngredientTab);
            tabPane.getTabs().remove(storageSweetTab);
            tabPane.getTabs().remove(supplierTab);
            tabPane.getTabs().remove(supplyOrderTab);
            tabPane.getTabs().remove(clientTab);

            sweetTable.setEditable(false);
            insertSweetButton.setVisible(false);

            sweetIngredientTable.setEditable(false);
            insertSweetIngredientButton.setVisible(false);
            deleteSweetIngredientButton.setVisible(false);

            ingredientTable.setEditable(false);
            insertIngredientNameField.setVisible(false);
            insertIngredientUnitComboBox.setVisible(false);
            insertIngredientButton.setVisible(false);

            clientOrderTable.setEditable(false);
            insertClientButton.setVisible(false);

            clientOrderContentTable.setEditable(false);
            insertClientOrderContentButton.setVisible(false);
            deleteClientOrderContentButton.setVisible(false);

            workshopTable.setEditable(false);
            insertWorkshopNameField.setVisible(false);
            insertWorkshopAddressField.setVisible(false);
            insertWorkshopButton.setVisible(false);

            positionTable.setEditable(false);
            insertPositionButton.setVisible(false);

            employeeTable.setEditable(false);
            insertEmployeeButton.setVisible(false);
        } else if(user.getAccess().getAbbr().equals("sale")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(categoryAndUnitTab);
            tabPane.getTabs().remove(ingredientTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(storageIngredientTab);
            tabPane.getTabs().remove(storageSweetTab);
            tabPane.getTabs().remove(supplierTab);
            tabPane.getTabs().remove(supplyOrderTab);
            tabPane.getTabs().remove(workshopProductionShiftTab);
            tabPane.getTabs().remove(positionEmployeeTab);

            sweetTable.setEditable(false);
            insertSweetButton.setVisible(false);
            sweetIngredientTable.setEditable(false);
            insertSweetIngredientButton.setVisible(false);
            deleteSweetIngredientButton.setVisible(false);
        } else if(user.getAccess().getAbbr().equals("purchase")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(sweetTab);
            tabPane.getTabs().remove(categoryAndUnitTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(storageSweetTab);
            tabPane.getTabs().remove(clientTab);
            tabPane.getTabs().remove(clientOrderTab);
            tabPane.getTabs().remove(workshopProductionShiftTab);
            tabPane.getTabs().remove(positionEmployeeTab);

            ingredientTable.setEditable(false);
            insertIngredientNameField.setVisible(false);
            insertIngredientUnitComboBox.setVisible(false);
            insertIngredientButton.setVisible(false);

            ingredientStorageTable.setEditable(false);
            insertIngredientStorageButton.setVisible(false);

            storedIngredientTable.setEditable(false);
            insertStoredIngredientButton.setVisible(false);
            deleteStoredIngredientButton.setVisible(false);
        } else if(user.getAccess().getAbbr().equals("storage")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(categoryAndUnitTab);
            tabPane.getTabs().remove(sweetTab);
            tabPane.getTabs().remove(ingredientTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(supplierTab);
            tabPane.getTabs().remove(supplyOrderTab);
            tabPane.getTabs().remove(clientTab);
            tabPane.getTabs().remove(clientOrderTab);
            tabPane.getTabs().remove(workshopProductionShiftTab);
            tabPane.getTabs().remove(positionEmployeeTab);
        } else if(user.getAccess().getAbbr().equals("sweet")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(storageIngredientTab);
            tabPane.getTabs().remove(storageSweetTab);
            tabPane.getTabs().remove(supplierTab);
            tabPane.getTabs().remove(supplyOrderTab);
            tabPane.getTabs().remove(clientTab);
            tabPane.getTabs().remove(clientOrderTab);
            tabPane.getTabs().remove(workshopProductionShiftTab);
            tabPane.getTabs().remove(positionEmployeeTab);
        } else if(user.getAccess().getAbbr().equals("staff")) {
            tabPane.getTabs().remove(userTab);
            tabPane.getTabs().remove(orderStatusTab);
            tabPane.getTabs().remove(categoryAndUnitTab);
            tabPane.getTabs().remove(sweetTab);
            tabPane.getTabs().remove(ingredientTab);
            tabPane.getTabs().remove(storageIngredientTab);
            tabPane.getTabs().remove(storageSweetTab);
            tabPane.getTabs().remove(supplierTab);
            tabPane.getTabs().remove(supplyOrderTab);
            tabPane.getTabs().remove(clientTab);
            tabPane.getTabs().remove(clientOrderTab);
            tabPane.getTabs().remove(workshopProductionShiftTab);
        }
    }

    public void setTableViewItems() {
        userTable.setItems(userList);
        categoryTable.setItems(categoryList);
        unitTable.setItems(unitList);
        sweetTable.setItems(sweetList);
        ingredientTable.setItems(ingredientList);
        orderStatusTable.setItems(orderStatusList);
        ingredientStorageTable.setItems(ingredientStorageList);
        sweetStorageTable.setItems(sweetStorageList);
        supplierTable.setItems(supplierList);
        supplyOrderTable.setItems(supplyOrderList);
        supplyOrderContentOrderTable.setItems(supplyOrderList);
        clientTable.setItems(clientList);
        clientOrderTable.setItems(clientOrderList);
        clientOrderContentOrderTable.setItems(clientOrderList);
        workshopTable.setItems(workshopList);
        productionTable.setItems(productionList);
        shiftTypeTable.setItems(shiftTypeList);
        shiftTable.setItems(shiftList);
        rateTypeTable.setItems(rateTypeList);
        positionTable.setItems(positionList);
        employeeTable.setItems(employeeList);
    }

    private <T> Callback<TableColumn<T, Date>, TableCell<T, Date>> dateCustomCellFactory() {
        return param -> new TableCell<>() {

            @Override
            public void updateItem(final Date item, boolean empty) {
                if (item != null) {
                    setText(Utils.formatDate(item));
                }
            }
        };
    }

    public void addExternalItem(Sweet sweet) {
        try {
            sweet = sweetRepository.save(sweet);
            sweetList.add(sweet);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }
    public void addExternalItem(Sweet sweet, List<SweetIngredient> ingredients) {
        try {
            Object[] objects = sweetService.save(sweet, ingredients);
            sweetList.add((Sweet) objects[0]);
            sweetIngredientList.addAll((List<SweetIngredient>) objects[1]);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(SweetIngredient sweetIngredient) {
        try {
            sweetIngredient = sweetIngredientRepository.save(sweetIngredient);
            SweetIngredient finalSweetIngredient = sweetIngredient;
            sweetIngredientList.add(finalSweetIngredient);
            Sweet sweet = sweetList.stream().filter(x -> x.getId() == finalSweetIngredient.getSweet().getId()).findFirst().get();
            sweetIngredientTable.setItems(FXCollections.observableList(sweetIngredientList.stream().filter(x -> x.getSweet().getId() == sweet.getId()).collect(Collectors.toList())));
        } catch(Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(IngredientStorage ingredientStorage) {
        try {
            ingredientStorage = ingredientStorageRepository.save(ingredientStorage);
            ingredientStorageList.add(ingredientStorage);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(StoredIngredient storedIngredient) {
        try {
            storedIngredient = storedIngredientRepository.save(storedIngredient);
            StoredIngredient finalStoredIngredient = storedIngredient;
            IngredientStorage ingredientStorage = ingredientStorageList.stream().filter(x -> x.getId() == finalStoredIngredient.getStorage().getId()).findFirst().get();
            ingredientStorage.getIngredients().add(storedIngredient);
            storedIngredientTable.setItems(FXCollections.observableList(ingredientStorage.getIngredients()));
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(SweetStorage sweetStorage) {
        try {
            sweetStorage = sweetStorageRepository.save(sweetStorage);
            sweetStorageList.add(sweetStorage);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(StoredSweet storedSweet) {
        try {
            storedSweet = storedSweetRepository.save(storedSweet);
            StoredSweet finalStoredSweet = storedSweet;
            SweetStorage sweetStorage = sweetStorageList.stream().filter(x -> x.getId() == finalStoredSweet.getStorage().getId()).findFirst().get();
            sweetStorage.getSweets().add(storedSweet);
            storedSweetTable.setItems(FXCollections.observableList(sweetStorage.getSweets()));
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(SupplyOrder supplyOrder) {
        try {
            supplyOrder = supplyOrderRepository.save(supplyOrder);
            supplyOrderList.add(supplyOrder);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(SupplyOrder supplyOrder, List<SupplyOrderContent> contents) {
        try {
            supplyOrder = supplyOrderService.save(supplyOrder, contents);
            supplyOrderList.add(supplyOrder);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(Position position) {
        try {
            position = positionRepository.save(position);
            positionList.add(position);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(Employee employee) {
        try {
            employee = employeeRepository.save(employee);
            employeeList.add(employee);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(SupplyOrderContent supplyOrderContent) {
        try {
            supplyOrderContent = supplyOrderContentRepository.save(supplyOrderContent);
            SupplyOrderContent finalSupplyOrderContent = supplyOrderContent;
            SupplyOrder supplyOrder = supplyOrderList.stream().filter(x -> x.getId() == finalSupplyOrderContent.getSupplyOrder().getId()).findFirst().get();
            supplyOrder.getContents().add(supplyOrderContent);
            supplyOrderContentTable.setItems(FXCollections.observableList(supplyOrder.getContents()));
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(ClientOrder clientOrder) {
        try {
            clientOrder = clientOrderRepository.save(clientOrder);
            clientOrderList.add(clientOrder);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(ClientOrder clientOrder, List<ClientOrderContent> contents) {
        try {
            clientOrder = clientOrderService.save(clientOrder, contents);
            clientOrderList.add(clientOrder);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(ClientOrderContent clientOrderContent) {
        try {
            clientOrderContent = clientOrderContentRepository.save(clientOrderContent);
            ClientOrderContent finalClientOrderContent = clientOrderContent;
            ClientOrder clientOrder = clientOrderList.stream().filter(x -> x.getId() == finalClientOrderContent.getClientOrder().getId()).findFirst().get();
            clientOrder.getContents().add(clientOrderContent);
            clientOrderContentTable.setItems(FXCollections.observableList(clientOrder.getContents()));
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(Production production) {
        try {
            production = productionRepository.save(production);
            productionList.add(production);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(Shift shift) {
        try {
            shift = shiftRepository.save(shift);
            shiftList.add(shift);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(Shift shift, List<ShiftEmployee> shiftEmployees) {
        try {
            shift = shiftService.save(shift, shiftEmployees);
            shiftList.add(shift);
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    public void addExternalItem(ShiftEmployee shiftEmployee) {
        try {
            shiftEmployee = shiftEmployeeRepository.save(shiftEmployee);
            ShiftEmployee finalShiftEmployee = shiftEmployee;
            Shift shift = shiftList.stream().filter(x -> x.getId() == finalShiftEmployee.getShift().getId()).findFirst().get();
            shift.getEmployees().add(shiftEmployee);
            shiftEmployeeTable.setItems(FXCollections.observableList(shift.getEmployees()));
        } catch (Exception e) {
            Utils.showAlert("Помилка", "Помилка збереження даних", Alert.AlertType.ERROR);
        }
    }

    private <T> void applyUpdates(Set<T> set, JpaRepository<T, Integer> repository) {
        if(!set.isEmpty()) {
            set.forEach(repository::save);
            set.clear();
        }
    }

    private void applyUpdates(Set<SupplyOrder> set, SupplyOrderRepository repository) {
        if(!set.isEmpty()) {
            set.forEach(x -> {
                if (x.getStatus().equals(orderStatusRepository.findByName("Виконано")) && x.getDeliveryDate() == null) {
                    Utils.showAlert("Помилка", "В одного або декількох змінених записів встановлено стан \"Виконано\", але не вказано дату доставки",
                            Alert.AlertType.ERROR);
                } else {
                    repository.save(x);
                    set.remove(x);
                }
            });
            supplierList.clear();
            supplierList.addAll(supplierRepository.findAll());
        }
    }

    private void applyUpdates(Set<ClientOrder> set, ClientOrderRepository repository) {
        if(!set.isEmpty()) {
            set.forEach(x -> {
                if (x.getStatus().equals(orderStatusRepository.findByName("Виконано")) && x.getDeliveryDate() == null) {
                    Utils.showAlert("Помилка", "В одного або декількох змінених записів встановлено стан \"Виконано\", але не вказано дату доставки",
                            Alert.AlertType.ERROR);
                } else {
                    repository.save(x);
                    set.remove(x);
                }
            });
            clientList.clear();
            clientList.addAll(clientRepository.findAll());
        }
    }

    private void openAddWindow(String name) {
        Stage stage = new Stage();
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(name));
            loader.setControllerFactory(context::getBean);
            root = loader.load();
            stage.setScene(new Scene(root));
            stage.setTitle("Sweet Business");
            stage.getIcons().add(new Image("img/icon.png"));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            Utils.showAlert("Помилка", "Не вдалось відкрити файл розмітки графічного інтерфейсу", Alert.AlertType.ERROR);
        }
    }
}
