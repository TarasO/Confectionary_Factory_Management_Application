package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.StoredSweet;
import com.oliinyk.kursova.entity.Sweet;
import com.oliinyk.kursova.entity.SweetStorage;
import com.oliinyk.kursova.repository.SweetRepository;
import com.oliinyk.kursova.repository.SweetStorageRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class StoredSweetController {

    @FXML private ComboBox<SweetStorage> insertStoredSweetStorageComboBox;
    @FXML private ComboBox<Sweet> insertStoredSweetSweetComboBox;
    @FXML private TextField insertStoredSweetQuantityField;
    @FXML private Button insertStoredSweetButton;

    private final SweetStorageRepository sweetStorageRepository;
    private final SweetRepository sweetRepository;

    private final MainController mainController;

    public StoredSweetController(SweetStorageRepository sweetStorageRepository, SweetRepository sweetRepository,
                                      MainController mainController) {
        this.sweetStorageRepository = sweetStorageRepository;
        this.sweetRepository = sweetRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertStoredSweetStorageComboBox.setItems(FXCollections.observableList(sweetStorageRepository.findAll()));
        insertStoredSweetSweetComboBox.setItems(FXCollections.observableList(sweetRepository.findAll()));

        insertStoredSweetButton.setOnAction(event -> {
            if(insertStoredSweetStorageComboBox.getSelectionModel().isEmpty()
                    || insertStoredSweetSweetComboBox.getSelectionModel().isEmpty()
                    || insertStoredSweetQuantityField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                StoredSweet storedSweet = new StoredSweet(insertStoredSweetStorageComboBox.getValue(),
                        insertStoredSweetSweetComboBox.getValue(), Double.parseDouble(insertStoredSweetQuantityField.getText()));
                mainController.addExternalItem(storedSweet);
            }
        });
    }
}
