package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.IngredientStorage;
import com.oliinyk.kursova.entity.Position;
import com.oliinyk.kursova.entity.RateType;
import com.oliinyk.kursova.repository.RateTypeRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class PositionController {

    @FXML private TextField insertPositionNameField;
    @FXML private TextField insertPositionAbbrField;
    @FXML private ComboBox<RateType> insertPositionRateTypeComboBox;
    @FXML private TextField insertPositionPaymentField;
    @FXML private TextField insertPositionNightBonusField;
    @FXML private TextField insertPositionLoyaltyBonusField;
    @FXML private Button insertPositionButton;

    private final RateTypeRepository rateTypeRepository;
    private final MainController mainController;

    public PositionController(RateTypeRepository rateTypeRepository, MainController mainController) {
        this.rateTypeRepository = rateTypeRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertPositionRateTypeComboBox.setItems(FXCollections.observableList(rateTypeRepository.findAll()));

        insertPositionButton.setOnAction(event -> {
            if(insertPositionNameField.getText().isBlank() || insertPositionAbbrField.getText().isBlank()
                    || insertPositionRateTypeComboBox.getSelectionModel().isEmpty() || insertPositionPaymentField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                int nightShiftBonus = 0;
                int loyaltyBonus = 0;
                if(!insertPositionNightBonusField.getText().isBlank()) {
                    nightShiftBonus = Integer.parseInt(insertPositionNightBonusField.getText());
                }
                if(!insertPositionLoyaltyBonusField.getText().isBlank()) {
                    loyaltyBonus = Integer.parseInt(insertPositionLoyaltyBonusField.getText());
                }
                Position position = new Position(insertPositionNameField.getText(), insertPositionAbbrField.getText(),
                        insertPositionRateTypeComboBox.getValue(), Double.parseDouble(insertPositionPaymentField.getText()),
                        nightShiftBonus, loyaltyBonus);
                mainController.addExternalItem(position);
            }
        });
    }
}
