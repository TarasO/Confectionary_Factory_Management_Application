package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.Ingredient;
import com.oliinyk.kursova.entity.IngredientStorage;
import com.oliinyk.kursova.entity.StoredIngredient;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.IngredientStorageRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class StoredIngredientController {

    @FXML private ComboBox<IngredientStorage> insertStoredIngredientStorageComboBox;
    @FXML private ComboBox<Ingredient> insertStoredIngredientIngredientComboBox;
    @FXML private TextField insertStoredIngredientQuantityField;
    @FXML private Button insertStoredIngredientButton;

    private final IngredientStorageRepository ingredientStorageRepository;
    private final IngredientRepository ingredientRepository;

    private final MainController mainController;

    public StoredIngredientController(IngredientStorageRepository ingredientStorageRepository, IngredientRepository ingredientRepository,
                                     MainController mainController) {
        this.ingredientStorageRepository = ingredientStorageRepository;
        this.ingredientRepository = ingredientRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertStoredIngredientStorageComboBox.setItems(FXCollections.observableList(ingredientStorageRepository.findAll()));
        insertStoredIngredientIngredientComboBox.setItems(FXCollections.observableList(ingredientRepository.findAll()));

        insertStoredIngredientButton.setOnAction(event -> {
            if(insertStoredIngredientStorageComboBox.getSelectionModel().isEmpty()
                    || insertStoredIngredientIngredientComboBox.getSelectionModel().isEmpty()
                    || insertStoredIngredientQuantityField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                StoredIngredient storedIngredient = new StoredIngredient(insertStoredIngredientStorageComboBox.getValue(),
                        insertStoredIngredientIngredientComboBox.getValue(), Double.parseDouble(insertStoredIngredientQuantityField.getText()));
                mainController.addExternalItem(storedIngredient);
            }
        });
    }
}
