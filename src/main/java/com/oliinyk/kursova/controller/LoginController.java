package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.User;
import com.oliinyk.kursova.repository.UserRepository;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LoginController {

    @FXML
    private TextField usernameField;

    @FXML
    private TextField passwordField;

    @FXML
    private Button loginButton;

    private final ConfigurableApplicationContext context;

    private final UserRepository userRepository;

    public LoginController(ConfigurableApplicationContext context, UserRepository userRepository) {
        this.context = context;
        this.userRepository = userRepository;
    }

    @FXML
    private void initialize() {
        loginButton.setOnAction(event -> {
            String username = usernameField.getText();
            String password = passwordField.getText();
            User user = userRepository.findByUsername(username);
            if(user != null) {
                if(user.getPassword().equals(password)) {
                    Stage stage = (Stage)loginButton.getScene().getWindow();
                    Parent root;
                    try {

                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
                        loader.setControllerFactory(context::getBean);
                        root = loader.load();

                        MainController mainController = loader.getController();
                        mainController.setUser(user);
                        mainController.applyDataAccessLimitations();
                        mainController.applyDataRetrievingAccessLimitations();

                        stage.setScene(new Scene(root));
                    } catch (IOException e) {
                        Utils.showAlert("Помилка", "Не вдалось відкрити файл розмітки графічного інтерфейсу", Alert.AlertType.ERROR);
                    }
                } else {
                    Utils.showAlert("Помилка", "Невірний пароль", Alert.AlertType.ERROR);
                }
            } else {
                Utils.showAlert("Помилка", "Користувача не знайдено", Alert.AlertType.ERROR);
            }
        });
    }
}
