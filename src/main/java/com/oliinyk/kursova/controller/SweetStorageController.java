package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.SweetStorage;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class SweetStorageController {

    @FXML private TextField insertSweetStorageNameField;
    @FXML private TextField insertSweetStorageAddressField;
    @FXML private Button insertSweetStorageButton;

    private final MainController mainController;

    public SweetStorageController(MainController mainController) {
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertSweetStorageButton.setOnAction(event -> {
            if(insertSweetStorageNameField.getText().isBlank() || insertSweetStorageAddressField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                SweetStorage sweetStorage = new SweetStorage(insertSweetStorageNameField.getText(),
                        insertSweetStorageAddressField.getText());
                mainController.addExternalItem(sweetStorage);
            }
        });
    }
}
