package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.IngredientRepository;
import com.oliinyk.kursova.repository.OrderStatusRepository;
import com.oliinyk.kursova.repository.SupplierRepository;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class SupplyOrderController {

    @FXML private TextField insertSupplyOrderIdentifierField;
    @FXML private ComboBox<Supplier> insertSupplyOrderSupplierComboBox;
    @FXML private DatePicker insertSupplyOrderDatePicker;
    @FXML private DatePicker insertSupplyOrderDeadlineDatePicker;
    @FXML private Button insertSupplyOrderButton;

    @FXML private TableView<SupplyOrderContent> insertSupplyOrderContentTable;
    @FXML private TableColumn<SupplyOrderContent, Ingredient> insertSupplyOrderContentIngredientColumn;
    @FXML private TableColumn<SupplyOrderContent, Double> insertSupplyOrderContentQuantityColumn;
    @FXML private TableColumn<SupplyOrderContent, Double> insertSupplyOrderContentPriceColumn;

    @FXML private ComboBox<Ingredient> insertSupplyOrderContentIngredientComboBox;
    @FXML private TextField insertSupplyOrderContentQuantityField;
    @FXML private TextField insertSupplyOrderContentPriceField;
    @FXML private Button insertSupplyOrderContentButton;

    private final IngredientRepository ingredientRepository;
    private final SupplierRepository supplierRepository;
    private final OrderStatusRepository orderStatusRepository;

    private final MainController mainController;

    public SupplyOrderController(IngredientRepository ingredientRepository, SupplierRepository supplierRepository,
                                 OrderStatusRepository orderStatusRepository, MainController mainController) {
        this.ingredientRepository = ingredientRepository;
        this.supplierRepository = supplierRepository;
        this.orderStatusRepository = orderStatusRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertSupplyOrderSupplierComboBox.setItems(FXCollections.observableList(supplierRepository.findAll()));

        ObservableList<Ingredient> ingredientList = FXCollections.observableList(ingredientRepository.findAll());
        insertSupplyOrderContentIngredientComboBox.setItems(ingredientList);

        //TableColumn
        insertSupplyOrderContentIngredientColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getIngredient()));
        insertSupplyOrderContentIngredientColumn.setCellFactory(ComboBoxTableCell.forTableColumn(ingredientList));
        insertSupplyOrderContentIngredientColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setIngredient(cellEditEvent.getNewValue());
        });

        insertSupplyOrderContentQuantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        insertSupplyOrderContentQuantityColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        insertSupplyOrderContentQuantityColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setQuantity(cellEditEvent.getNewValue());
        });

        insertSupplyOrderContentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        insertSupplyOrderContentPriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        insertSupplyOrderContentPriceColumn.setOnEditCommit(cellEditEvent -> {
            SupplyOrderContent supplyOrderContent = cellEditEvent.getTableView().getItems().get(cellEditEvent.getTablePosition().getRow());
            supplyOrderContent.setPrice(cellEditEvent.getNewValue());
        });

        //Button
        insertSupplyOrderContentButton.setOnAction(event -> {
            if(insertSupplyOrderContentIngredientComboBox.getSelectionModel().isEmpty()
                    || insertSupplyOrderContentQuantityField.getText().isEmpty() || insertSupplyOrderContentPriceField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                insertSupplyOrderContentTable.getItems().add(new SupplyOrderContent(insertSupplyOrderContentIngredientComboBox.getValue(),
                                                                Double.parseDouble(insertSupplyOrderContentQuantityField.getText()),
                                                                Double.parseDouble(insertSupplyOrderContentPriceField.getText())));
            }
        });

        insertSupplyOrderButton.setOnAction(event -> {
            if(insertSupplyOrderIdentifierField.getText().isBlank() || insertSupplyOrderSupplierComboBox.getSelectionModel().isEmpty()
                    || insertSupplyOrderDatePicker.getValue() == null || insertSupplyOrderDeadlineDatePicker.getValue() == null) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                SupplyOrder supplyOrder = new SupplyOrder(insertSupplyOrderIdentifierField.getText(),
                        insertSupplyOrderSupplierComboBox.getValue(), orderStatusRepository.findByName("Замовлено"),
                        Date.valueOf(insertSupplyOrderDatePicker.getValue()), Date.valueOf(insertSupplyOrderDeadlineDatePicker.getValue()));
                if(insertSupplyOrderContentTable.getItems().isEmpty()) {
                    mainController.addExternalItem(supplyOrder);
                } else {
                    mainController.addExternalItem(supplyOrder, insertSupplyOrderContentTable.getItems());
                }
            }
        });
    }
}
