package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.Employee;
import com.oliinyk.kursova.entity.Position;
import com.oliinyk.kursova.entity.RateType;
import com.oliinyk.kursova.entity.Workshop;
import com.oliinyk.kursova.repository.PositionRepository;
import com.oliinyk.kursova.repository.RateTypeRepository;
import com.oliinyk.kursova.repository.WorkshopRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class EmployeeController {

    @FXML private TextField insertEmployeeLastNameField;
    @FXML private TextField insertEmployeeFirstNameField;
    @FXML private DatePicker insertEmployeeBirthDatePicker;
    @FXML private ComboBox<Position> insertEmployeePositionComboBox;
    @FXML private ComboBox<Workshop> insertEmployeeWorkshopComboBox;
    @FXML private TextField insertEmployeePhoneNumberField;
    @FXML private DatePicker insertEmployeeBeginDatePicker;
    @FXML private Button insertEmployeeButton;

    private final PositionRepository positionRepository;
    private final WorkshopRepository workshopRepository;
    private final MainController mainController;

    public EmployeeController(PositionRepository positionRepository, WorkshopRepository workshopRepository,
                              MainController mainController) {
        this.positionRepository = positionRepository;
        this.workshopRepository = workshopRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertEmployeePositionComboBox.setItems(FXCollections.observableList(positionRepository.findAll()));
        insertEmployeeWorkshopComboBox.setItems(FXCollections.observableList(workshopRepository.findAll()));

        insertEmployeeButton.setOnAction(event -> {
            if(insertEmployeeLastNameField.getText().isBlank() || insertEmployeeFirstNameField.getText().isBlank()
                    || insertEmployeeBirthDatePicker.getValue() == null || insertEmployeePositionComboBox.getSelectionModel().isEmpty()
                    || insertEmployeeWorkshopComboBox.getSelectionModel().isEmpty() || insertEmployeePhoneNumberField.getText().isBlank()
                    || insertEmployeeBeginDatePicker.getValue() == null) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Employee employee = new Employee(insertEmployeeLastNameField.getText(), insertEmployeeFirstNameField.getText(),
                        Date.valueOf(insertEmployeeBirthDatePicker.getValue()), insertEmployeePositionComboBox.getValue(),
                        insertEmployeeWorkshopComboBox.getValue(), insertEmployeePhoneNumberField.getText(),
                        Date.valueOf(insertEmployeeBeginDatePicker.getValue()));
                mainController.addExternalItem(employee);
            }
        });
    }
}
