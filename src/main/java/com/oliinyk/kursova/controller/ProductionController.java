package com.oliinyk.kursova.controller;

import com.oliinyk.kursova.Utils;
import com.oliinyk.kursova.entity.*;
import com.oliinyk.kursova.repository.RateTypeRepository;
import com.oliinyk.kursova.repository.ShiftRepository;
import com.oliinyk.kursova.repository.SweetRepository;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class ProductionController {

    @FXML private TextField insertProductionIdentifierField;
    @FXML private ComboBox<Sweet> insertProductionSweetComboBox;
    @FXML private ComboBox<Shift> insertProductionShiftComboBox;
    @FXML private TextField insertProductionQuantityField;
    @FXML private Button insertProductionButton;

    private final SweetRepository sweetRepository;
    private final ShiftRepository shiftRepository;
    private final MainController mainController;

    public ProductionController(SweetRepository sweetRepository, ShiftRepository shiftRepository, MainController mainController) {
        this.sweetRepository = sweetRepository;
        this.shiftRepository = shiftRepository;
        this.mainController = mainController;
    }

    @FXML
    private void initialize() {
        insertProductionSweetComboBox.setItems(FXCollections.observableList(sweetRepository.findAll()));
        insertProductionShiftComboBox.setItems(FXCollections.observableList(shiftRepository.findAll()));

        insertProductionButton.setOnAction(event -> {
            if(insertProductionIdentifierField.getText().isBlank() || insertProductionSweetComboBox.getSelectionModel().isEmpty()
                    || insertProductionShiftComboBox.getSelectionModel().isEmpty() || insertProductionQuantityField.getText().isBlank()) {
                Utils.showAlert("Помилка", "Одне або декілька полів порожні", Alert.AlertType.ERROR);
            } else {
                Production production = new Production(insertProductionIdentifierField.getText(), insertProductionSweetComboBox.getValue(),
                        insertProductionShiftComboBox.getValue(), Double.parseDouble(insertProductionQuantityField.getText()));
                mainController.addExternalItem(production);
            }
        });
    }
}
